/**
 * This example plugin pops up a dialog box that says "Hello, world!".
 * 
 * Copyright University of California
 * 
 * LICENSE:      This file is distributed under the BSD license.
 *               License text is included with the source distribution.
 *
 *               This file is distributed in the hope that it will be useful,
 *               but WITHOUT ANY WARRANTY; without even the implied warranty
 *               of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *               IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 *               CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *               INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES.
 */

package uniGoettingen.DPI.ism;

import java.awt.EventQueue;
import java.awt.Transparency;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.AbstractCellEditor;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import mmcorej.CMMCore;
import mmcorej.Metadata;
import mmcorej.StrVector;
import mmcorej.TaggedImage;

import org.micromanager.acquisition.AcquisitionEngine;
import org.micromanager.api.IAcquisitionEngine2010;
import org.micromanager.api.ImageCache;
import org.micromanager.api.MMPlugin;
import org.micromanager.api.ScriptInterface;
import org.micromanager.api.SequenceSettings;
import org.micromanager.dialogs.ChannelTableModel;
import org.micromanager.utils.AcqOrderMode;
import org.micromanager.utils.ChannelSpec;
import org.micromanager.utils.DisplayMode;
import org.micromanager.utils.MMException;
import org.micromanager.utils.MMScriptException;
import org.micromanager.utils.NumberUtils;
import org.micromanager.utils.ReportingUtils;



public class ISMPlugin implements MMPlugin {
   public static final String menuName = "CSDISM";
   public static final String tooltipDescription =
      "Displays a simple dialog";
	private JFrame frame;
   // Provides access to the Micro-Manager Java API (for GUI control and high-
   // level functions).
   private ScriptInterface app_;
   // Provides access to the Micro-Manager Core API (for direct hardware
   // control)
   private CMMCore core_;
   public CMMCore core1;

   
   private AcquisitionEngine acqEng_;
   ISMControlGUI ISMControl = null;
	
   @Override
   public void setApp(ScriptInterface app) {
      app_ = app;
      core_ = app.getMMCore();
      core1 = core_;
      
   	   
	  //System.loadLibrary("ISMcore"); // hello.dll (Windows) or libhello.so (Unixes)
   
   }

   @Override
   public void dispose() {
      // We do nothing here as the only object we create, our dialog, should
      // be dismissed by the user.
   }

   @Override
   public void show() {
	  //String str = FPGAfuns.Laser();
      //JOptionPane.showMessageDialog(null, str, "ISM", JOptionPane.PLAIN_MESSAGE);
	   /*
	   EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					//core1.loadDevice("Andor Camera", "Andor", "Andor");
					//ISMControlCenter window = new ISMControlCenter(core1);
					//window.frame.setVisible(true);
					//StrVector devices = core_.getLoadedDevices();
					//System.out.println("Device status:");
					
										
					
					
					//core1.getDeviceName("Andor");
					
					//CMMCore core = new CMMCore();
					//core_.loadDevice("DPI_ISM", "ISM", "ISMcontrol");
					//core_.loadDevice("Camera", "DemoCamera", "DCam");


				
			      //final SequenceSettings sqsetting = new SequenceSettings();
			      //sqsetting.numFrames = 20;
			      //core_.setExposure(200);
			      
			      /*
			      Thread t = new Thread(new Runnable() {
			    	    public void run() {
							//app_.setAcquisitionSettings(sqsetting);
							
							try {
								//core_.prepareSequenceAcquisition("Camera");
								//core_.initializeCircularBuffer(); 
								//core_.startSequenceAcquisition(20, 200, true);
								ISMImageProcessor imgprocessor = new ISMImageProcessor(sqsetting);
								app_.addImageProcessor(imgprocessor);
								String acqname = app_.runAcquisition();
								app_.addToAlbum(imgprocessor.getAveImage());
								

								app_.removeImageProcessor(imgprocessor);

								//TaggedImage images =  core_.getTaggedImage(21);
								//Metadata md = null;
								//short[] metadata = (short[]) core_.getLastImageMD(md);
								//System.out.println("metadata length= " + metadata.length);
								int bufcap = core_.getBufferTotalCapacity();
								 System.out.println("BufferTotalCapacity = " + bufcap);
								
								
								while(core_.isSequenceRunning() && core_.getRemainingImageCount() > 0 ){
									 Object frames = core_.popNextImage(); 
									 System.out.println(core_.getRemainingImageCount());
								}
								
								while(core_.isSequenceRunning("Camera")){
									 Object frames = core_.popNextImage(); 
									 System.out.println(core_.getRemainingImageCount());
								}								
							
							
							
								//String acqname = app_.runAcquisition();
								//core_.startContinuousSequenceAcquisition(100);
								core_.startSequenceAcquisition("Camera", 20, 100, false);
								
								//while(false){
									//System.out.println(core_.getRemainingImageCount());
									//Object frames = core_.getLastImage();
									
								    //mdata = MMCorePy.Metadata();
								    //img[i] = core_.getNBeforeLastImageMD(i,0,mdata); 
								    //md[i] = mdata;
								
								//}
								//ImageCache img = app_.getAcquisitionImageCache(acqname);
								int height = (int) core_.getImageHeight();
								int width = (int) core_.getImageWidth();
								TaggedImage taggleimg = core_.getTaggedImage();
								//TaggedImage taggleimg = core_.getTaggedImage();
								int BufferSize = core_.getImageBufferSize();
								String camdev = core_.getCameraDevice();
								int Nchanel = (int) core_.getNumberOfCameraChannels();
								TaggedImage taggleimg1 = core_.getTaggedImage(Nchanel);
								short[] lastImg = (short[]) core_.getLastImage();
								System.out.println("Last image length = " + lastImg.length);
								//app_.getAcquisitionImageCache(acqname).getImage(arg0, arg1, arg2, arg3)
								
					            short[] img1 = (short[]) taggleimg.pix;
					            for(int i = 0; i< img1.length; i= i+ 1)
					            {
					            	img1[i] = (short) (img1[i] + 10);
					            }
				            	System.out.println("length = " + img1.length);
								System.out.println("height = " + height);
								System.out.println("width = " + width);
								System.out.println("BufferSize = " + BufferSize);
								System.out.println("Is taggleimg equal: " + taggleimg.equals(taggleimg1));
									
								
							} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								
								
								
								//img.getImage(arg0, arg1, arg2, arg3);
				
			    	        // stuff here
			    	    }
			    	});
			    	t.start();
					//app_.getAcqDlg();
					
				    
				    

					
					//String IsmPropty = core_.getProperty("Andor Camera", "Exposure");
					//core_.setProperty("Andor Camera", "Exposure", 100);
					//core_.setExposure("Andor Camera", 100);
					//core_.
					//core_.loadDevice("DPI_ISM", "FreeSerialPort", "FreeSerialPort");
					//String IsmPropty = core_.getProperty("DPI_ISM", "Ni");
					System.out.println("--------------------------\n");
					//System.out.println(IsmPropty);
					System.out.println("--------------------------\n");
					//core_.setProperty(label, propName, propValue);
					//core_.initializeDevice(label);
					
				} catch (Exception e) {
					e.printStackTrace();
					
				}
			}
		});
	   
	   */

		
		if(ISMControl==null){
				ISMControl = new ISMControlGUI(app_, core1, this);	   					
			}
		ISMControl.frame.setVisible(true);	
	   
   }
   
   @Override
   public String getInfo () {
      return "Displays a simple greeting.";
   }

   @Override
   public String getDescription() {
      return tooltipDescription;
   }
   
   @Override
   public String getVersion() {
      return "1.0";
   }
   
   @Override
   public String getCopyright() {
      return "University of California, 2015";
   }
   

}
