package uniGoettingen.DPI.ism;

class lvFPGA{
	public native static void ChangeParameter(int PNi, int PNc, boolean EnL1, boolean EnL2, boolean EnL3, boolean EnL4, int PL1, int PL2,  int PL3, int PL4, int PM1, int PM2, int PNp,  int PNwp);
	public native static boolean StartImaging(int ni, int nc, boolean EnL1, boolean EnL2, boolean EnL3, boolean EnL4, int iL1, int iL2, int iL3, int iL4, int m1, int m2, int np, int nwp, boolean setPar);
	public native static void StopImaging();
	public native static void CloseImaging();
	public native static int GetNumImageTaken();
}
