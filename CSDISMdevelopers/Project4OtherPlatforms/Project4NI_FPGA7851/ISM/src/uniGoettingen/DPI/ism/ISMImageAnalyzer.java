package uniGoettingen.DPI.ism;

import java.awt.Color;

import javax.swing.JTextField;

import org.json.JSONObject;
import org.micromanager.api.SequenceSettings;
import org.micromanager.api.TaggedImageAnalyzer;

import ij.IJ;
import ij.ImagePlus;
import ij.gui.NewImage;
import ij.plugin.filter.RankFilters;
import ij.process.Blitter;
import ij.process.ImageProcessor;
import mmcorej.TaggedImage;

public class ISMImageAnalyzer extends TaggedImageAnalyzer  {
	
	
	

	private int n = 0;
	int[] aveimg = null;
	private short[] image;
	private int N;
	JSONObject tag =null;
	TaggedImage taggleimage0 = null;
	JTextField textField_Nimg;
	
	ISMImageAnalyzer(SequenceSettings seqsetting, JTextField TextField){
		n = 0;
		N = seqsetting.numFrames;
		aveimg = null;
		textField_Nimg = TextField;
	}
	
	
	@Override
	protected void analyze(TaggedImage taggleimage) {
		// TODO Auto-generated method stub
		
		ImagePlus ImPlus;
		image = (short[]) taggleimage.pix;
		tag = taggleimage.tags;
		
		
		if(image != null && image.length > 1 && n<=N){
			
			n = n + 1;
			
			textField_Nimg.setText(Integer.toString(n));
			//System.out.println(Integer.toString(n));
			if(n==1){
				aveimg =  new int[image.length];
				//aveimg1 =  new char[image.length];
				
			}
			if(n==N){
				n = 0;
				taggleimage0 = new TaggedImage(image.clone(),taggleimage.tags);
			}
			
			Aveimage(image);
			
		}
		
	}

	void Aveimage(short[] image2){
		if(n>1){
			for(int i = 0; i<image2.length; i++){
				aveimg[i] = aveimg[i] + (image2[i] & 0x00000FFFF);				
			}
		}
		
	}
	
	public TaggedImage getAveImage(){
		
		int pixel;
		//taggleimage0.tags = tag;
		if(aveimg!=null && aveimg.length>1){
			
			
			if(taggleimage0 == null){
				return null;
			}
			
			short[] timg = (short[]) taggleimage0.pix;
			for(int i = 0; i<aveimg.length; i++){
				 pixel =  (aveimg[i]/N);
				 timg[i] =  (short) (pixel & 0x00000FFFF);
						 
			}
			
		}
		
		return taggleimage0;
	}
	
	public static  short[] normalize16(short[] image){
		short[] result = new short[image.length];
		int[] data = new int[image.length];
		int max =0, min = 0;
		for(int i = 0; i<image.length; i++){
			 data[i] = image[i] & 0x00000FFFF;
			 
			 if(data[i]>max){
				 max = data[i];
			 }
			   
			 if(data[i]<min){
				 min = data[i];
			 }
				 
		}
		if(min == max){
			return image;
		}
			
		for(int i = 0; i<image.length; i++){
			int pixel =  (65535*(data[i] - min)/(max - min));
			result[i] = (short) (pixel & 0x00000FFFF);
		}
		
		return result;
		
	}
	
	public static void Contrast(ImagePlus imp, int radius, boolean doIwhite) {
		// G. Landini, 2013
		// Based on a simple contrast toggle. This procedure does not have user-provided paramters other than the kernel radius
		// Sets the pixel value to either white or black depending on whether its current value is closest to the local Max or Min respectively
		// The procedure is similar to Toggle Contrast Enhancement (see Soille, Morphological Image Analysis (2004), p. 259

		ImagePlus Maximp, Minimp;
		ImageProcessor ip=imp.getProcessor(), ipMax, ipMin;
		int c_value =0;
		int mid_gray;
		short object;
		short backg;


		if (doIwhite){
			object =  (short) 0xffff;
			backg =   (short) 0;
		}
		else {
			object =  (short) 0;
			backg =  (short) 0xffff;
		}

		Maximp=duplicateImage(ip);
		ipMax=Maximp.getProcessor();
		RankFilters rf=new RankFilters();
		rf.rank(ipMax, radius, rf.MAX);// Maximum
		//Maximp.show();
		Minimp=duplicateImage(ip);
		ipMin=Minimp.getProcessor();
		rf.rank(ipMin, radius, rf.MIN); //Minimum
		//Minimp.show();
		short[] pixels = (short [])ip.getPixels();
		short[] max = (short [])ipMax.getPixels();
		short[] min = (short [])ipMin.getPixels();
		for (int i=0; i<pixels.length; i++) {
			pixels[i] = ((Math.abs((int)(max[i]&0xffff- pixels[i]&0xffff)) <= Math.abs((int)(pixels[i]&0xffff- min[i]&0xffff)))) ? object : backg;
		}    
		//imp.updateAndDraw();
		return;
	}
	
	private static ImagePlus duplicateImage(ImageProcessor iProcessor){
		int w=iProcessor.getWidth();
		int h=iProcessor.getHeight();
		ImagePlus iPlus=NewImage.createShortImage("Image", w, h, 1, NewImage.FILL_BLACK);
		ImageProcessor imageProcessor=iPlus.getProcessor();
		imageProcessor.copyBits(iProcessor, 0,0, Blitter.COPY);
		return iPlus;
	} 
	
}
