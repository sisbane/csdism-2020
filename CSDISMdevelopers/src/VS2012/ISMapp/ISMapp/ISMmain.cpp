/*
//     DLL project for SDC-ISM MicroManager Plugin: A open source software for Spinning Disk Confocal-Image Scanning microscopy image acquisition
// 
//     Copyright (C) 2019  Shun Qin, shun.qin@phys.uni-goettingen.de
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//     We expect you would cite our article, to which this source code project attach.
*/

#include <stdio.h>
#include "NiFpga.h"
#include "NiFpga_ISMcore.h"
#include <Windows.h>
#include <math.h>
#include "uniGoettingen_DPI_ism_lvFPGA.h"

NiFpga_Status status;
NiFpga_Session session;


static bool isInitialized = false;



void EnableFPGA(bool en)
{

	NiFpga_MergeStatus(&status, NiFpga_WriteBool(session, NiFpga_ISMcore_ControlBool_Enable, en));

}


void ResetFPGA(bool start)
{

	NiFpga_MergeStatus(&status, NiFpga_WriteBool(session, NiFpga_ISMcore_ControlBool_Start, start));

}




bool setParameters(unsigned int PNi, unsigned int PNc, bool EnL1, bool EnL2, bool EnL3, bool EnL4, unsigned int PL1, unsigned int PL2, unsigned int PL3, unsigned int PL4,
	unsigned int PM1, unsigned int PM2, unsigned int PNp, unsigned int PNwp)
{
	unsigned short int Ni, Nc, L1, L2, L3, L4, M1, M2, Np, No, Nwp;
	M1 = PM1;
	M2 = PM2;

	//laser internsity
	L1 = PL1;
	L2 = PL2;
	L3 = PL3;
	L4 = PL4;

	Np = PNp;      // number of laser pulse per spinning cycles	
	Ni = PNi; // number of images to be taken
	Nc = PNc;      // spinning cycles to take one image
	Nwp = PNwp;    //200*25ns = 5us
	No = 0;     //laser trigger shifting offset, is not used anymore
	NiFpga_IfIsNotError(status, NiFpga_WriteI16(session, NiFpga_ISMcore_ControlI16_DiskCyclesPerImage, Nc));
	NiFpga_IfIsNotError(status, NiFpga_WriteI16(session, NiFpga_ISMcore_ControlI16_ExpectedImages, Ni));



	NiFpga_IfIsNotError(status, NiFpga_WriteI8(session, NiFpga_ISMcore_ControlBool_EnL1, EnL1));
	NiFpga_IfIsNotError(status, NiFpga_WriteI8(session, NiFpga_ISMcore_ControlBool_EnL2, EnL2));
	NiFpga_IfIsNotError(status, NiFpga_WriteI8(session, NiFpga_ISMcore_ControlBool_EnL3, EnL3));
	NiFpga_IfIsNotError(status, NiFpga_WriteI8(session, NiFpga_ISMcore_ControlBool_EnL4, EnL4));
	


	NiFpga_IfIsNotError(status, NiFpga_WriteI16(session, NiFpga_ISMcore_ControlI16_LaserIntensity1, L1));
	NiFpga_IfIsNotError(status, NiFpga_WriteI16(session, NiFpga_ISMcore_ControlI16_LaserIntensity2, L2));
	NiFpga_IfIsNotError(status, NiFpga_WriteI16(session, NiFpga_ISMcore_ControlI16_LaserIntensity3, L3));
	NiFpga_IfIsNotError(status, NiFpga_WriteI16(session, NiFpga_ISMcore_ControlI16_LaserIntensity4, L4));


	NiFpga_IfIsNotError(status, NiFpga_WriteI16(session, NiFpga_ISMcore_ControlI16_M1, M1));
	NiFpga_IfIsNotError(status, NiFpga_WriteI16(session, NiFpga_ISMcore_ControlI16_M2, M2));

	NiFpga_IfIsNotError(status, NiFpga_WriteI16(session, NiFpga_ISMcore_ControlI16_NumofLaserPulse, Np));
	NiFpga_IfIsNotError(status, NiFpga_WriteI16(session, NiFpga_ISMcore_ControlI16_PulseShiftOffset, No));
	NiFpga_IfIsNotError(status, NiFpga_WriteI16(session, NiFpga_ISMcore_ControlI16_PulseWidth, Nwp));


	return true;
}




bool isFinished()
{
	// wait for finishing signal
	NiFpga_Bool Finished = false;


	NiFpga_MergeStatus(&status, NiFpga_ReadBool(session, NiFpga_ISMcore_IndicatorBool_Finished, &Finished));
	
	return Finished;

}


int GetNumImageTaken()
{
	short int n = 0;
	NiFpga_MergeStatus(&status, NiFpga_ReadI16(session, NiFpga_ISMcore_IndicatorI16_OutputImages, &n));
	return n;
}




bool Intialize()
{
	/*
	M1 = 0;
	M2 = 1;

	//laser internsity
	L1=20000;
	L2 = 0;
	L3 = 0;
	L4 = 0;

	Np = 5;      // number of laser pulse per spinning cycles	
	Ni = 250; // number of images to be taken
	Nc = 20;      // spinning cycles to take one image
	Nwp = 200;    //200*25ns = 5us
	No = 200;     //laser trigger shifting offset, is not used anymore
	*/
	status = NiFpga_Initialize();
	if (NiFpga_IsNotError(status))
	{
		//NiFpga_Session session;


		NiFpga_Status NiFpgaStatus = NiFpga_Open(NiFpga_ISMcore_Bitfile,	NiFpga_ISMcore_Signature, "RIO0", NiFpga_OpenAttribute_NoRun, &session);

		/* opens a session, downloads the bitstream, but does not run the FPGA */
		NiFpga_MergeStatus(&status, NiFpgaStatus);


		if (NiFpga_IsNotError(status))
		{
			NiFpga_MergeStatus(&status, NiFpga_Run(session, 0));

			ResetFPGA(false);
			EnableFPGA(true);
			//setParameters();

			return true;
		}
	}
	return false;
}



bool StartImaging(unsigned short Ni, unsigned short Nc, bool EnL1, bool EnL2, bool EnL3, bool EnL4, unsigned short L1, unsigned short L2, unsigned short L3, 
				  unsigned short L4, unsigned short M1, unsigned short M2, unsigned short Np, unsigned short Nwp, bool setPar)
{
	// FPGA has to be initialized before start the first imaging
	//isInitialized = false;
	//if (!flag)
	if(!isInitialized)
	{
		bool ret = Intialize();
		if(!ret)
		{
			return false;
		}
		
		isInitialized = true;
	}
	if(setPar){

		setParameters(Ni, Nc, EnL1, EnL2, EnL3, EnL4, L1, L2, L3, L4, M1, M2, Np, Nwp);
		Sleep(300);
	}
	//start imaging
	ResetFPGA(false);//reset all FPGA registers

	Sleep(10);
	ResetFPGA(true);  //FPGA core start to work
	//NiFpga_MergeStatus(&status, NiFpga_Run(session, 0));
	return true;
}

void StopImaging()
{
	
	//start imaging
	ResetFPGA(false);//reset all FPGA registers
	//NiFpga_MergeStatus(&status, NiFpga_Run(session, 0));

}

void CloseImaging()
{

	isInitialized = false;

	EnableFPGA(false);
	ResetFPGA(false);
	
	//NiFpga_MergeStatus(&status, NiFpga_Run(session, 0));
	NiFpga_MergeStatus(&status, NiFpga_Close(session, 0));//NiFpga_CloseAttribute_NoResetIfLastSession

	/* must be called after all other calls */
	NiFpga_MergeStatus(&status, NiFpga_Finalize());



	/* check if anything went wrong */
	if (NiFpga_IsError(status))
	{
		char error[32];
		sprintf_s(error, "Error %d!\n", status);
		fprintf(stderr, error);
	}	

	
		
}




/*
void main()
{
	//#define Tclk 25 //f_clk = 40Mhz T = 25ns
	//#define ToClkCycles(usecond) usecond*1000/Tclk
	//dT = (Ts - Np * Wp) / (Ni*Np); 
	//Tov = Wp - dT;
	//Ni = (Ts - Np*Wp) / ((1 - lambda)*Wp*Np); // if Tov = lambda*Wp
	//int k = ToClkCycles(25);

	double clk = 40000000.0;
	double Ts = (1 / 360.0 - 31/clk)*1000000 ; //in us, 31 cycle of clk is used for camera's activation
	//double Wp= 6.0;  // laster pulse wide in us



	double lambda = 1 / 4.0;
	//int No;
	//double dT;



	
	//Nwp = floor(Wp*1000/25.0);

	//Tov = Wp - dT;
	//Ni = (Ts - Np*Wp) / ((1 - lambda)*Wp*Np);
	//lambda = 1 - (Ts - Np*Wp) / (Wp*Np*Ni);
	//dT = (Ts - Np * Wp) / (Ni*Np); // laser pulse shifting delay
	//No = floor(dT * 1000 / 25.0);  // us to number of clock cycle(25ns), this parameter is not used any more
	
	unsigned short int Ni, Nc, L1, L2, L3, L4, M1, M2, Np, No, Nwp, EnL1, EnL2, EnL3, EnL4;
	M1 = 0;
	M2 = 10;

	//laser internsity
	L1 = 20000;
	L2 = 0;
	L3 = 0;
	L4 = 0;

	Np = 4;      // number of laser pulse per spinning cycles	
	Ni = 250; // number of images to be taken
	Nc = 2;      // spinning cycles to take one image
	Nwp = 240;    //200*25ns = 5us
	No = 0;     //laser trigger shifting offset, is not used anymore


	EnL1 = 1;
	EnL2 = 1;
	EnL3 = 1;
	EnL4 = 1; 


	//Intialize();
	//setParameters(Ni, Nc, EnL1, EnL2, EnL3, EnL4, L1, L2, L3, L4, M1, M2, Np, Nwp);

	for(int i=0; i<1; i++){
		bool ret = StartImaging(Ni, Nc, EnL1, EnL2, EnL3, EnL4, L1, L2, L3, L4, M1, M2, Np, Nwp, true);
		 while(true){
			 
			 int n = GetNumImageTaken();
			 printf("%d %d\n", n, i);
			 if(n>=Ni){
				 n = 0;
				break;
			 }
		 }

	}



	bool ret1 = StartImaging(Ni, Nc, EnL1, EnL2, EnL3, EnL4, L1, L2, L3, L4, M1, M2, Np, Nwp, true);
	while(!isFinished())
	{
		int n = GetNumImageTaken();
		if(n <= 10)
		printf("Current Num of images taken: %d \n,", n);
	}
	CloseImaging();
}




*/


/*
 * Class:     uniGoettingen_DPI_ism_lvFPGA
 * Method:    ChangeParameter
 * Signature: (IIIIIIIIII)V
 */
JNIEXPORT void JNICALL Java_uniGoettingen_DPI_ism_lvFPGA_ChangeParameter
(JNIEnv *env, jclass claxx, jint PNi, jint PNc, jboolean EnL1, jboolean EnL2, jboolean EnL3, jboolean EnL4, jint PL1, jint PL2, jint PL3, jint PL4, jint PM1, jint PM2, jint PNp, jint PNwp)
{
	unsigned short int Ni, Nc, L1, L2, L3, L4, M1, M2, Np, No, Nwp;
	M1 = (int)PM1;
	M2 = (int)PM2;

	//laser internsity
	L1 = (int)PL1;
	L2 = (int)PL2;
	L3 = (int)PL3;
	L4 = (int)PL4;

	Np = (int)PNp;      // number of laser pulse per spinning cycles	
	Ni = (int)PNi; // number of images to be taken
	Nc = (int)PNc;      // spinning cycles to take one image
	Nwp = (int)PNwp;    //200*25ns = 5us
	No = 0;     //laser trigger shifting offset, is not used anymore

	setParameters(Ni, Nc, EnL1, EnL2, EnL3, EnL4, L1, L2, L3, L4, M1, M2, Np, Nwp);


}
/*
 * Class:     uniGoettingen_DPI_ism_lvFPGA
 * Method:    StartImaging
 * Signature: ()V
 */
JNIEXPORT jboolean JNICALL Java_uniGoettingen_DPI_ism_lvFPGA_StartImaging(JNIEnv *env, jclass claxx, jint PNi, jint PNc, jboolean EnL1, jboolean EnL2, jboolean EnL3, jboolean EnL4, jint PL1, jint PL2, jint PL3, jint PL4, jint PM1, jint PM2, jint PNp, jint PNwp, jboolean setPar)
{
	unsigned short Ni, Nc, L1, L2, L3, L4, M1, M2, Np, Nwp;
	M1 = (int)PM1;
	M2 = (int)PM2;

	//laser internsity
	L1 = (int)PL1;
	L2 = (int)PL2;
	L3 = (int)PL3;
	L4 = (int)PL4;

	Np = (int)PNp;      // number of laser pulse per spinning cycles	
	Ni = (int)PNi; // number of images to be taken
	Nc = (int)PNc;      // spinning cycles to take one image
	Nwp = (int)PNwp;    //200*25ns = 5us

	if(M1<50 && M1>0){ // M1 = 0 is possible
		M1 = 50;       // 1us is not safe due to [-20, +20] ticks of the dynamic range of spinning cycle 
	}else if(M1>800){  // 20us is enough for camera activation, should not too much
		M1 = 800;
	}

	return StartImaging(Ni, Nc, EnL1, EnL2, EnL3, EnL4, L1, L2, L3, L4, M1, M2, Np, Nwp, setPar);
}

/*
 * Class:     uniGoettingen_DPI_ism_lvFPGA
 * Method:    StopImaging
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_uniGoettingen_DPI_ism_lvFPGA_StopImaging(JNIEnv *env, jclass claxx)
{

	StopImaging();
}

/*
 * Class:     uniGoettingen_DPI_ism_lvFPGA
 * Method:    CloseImaging
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_uniGoettingen_DPI_ism_lvFPGA_CloseImaging(JNIEnv *env, jclass claxx)
{
    CloseImaging();

}

/*
 * Class:     uniGoettingen_DPI_ism_lvFPGA
 * Method:    GetNumImageTaken
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_uniGoettingen_DPI_ism_lvFPGA_GetNumImageTaken(JNIEnv *env, jclass claxx)
{
		return GetNumImageTaken();

}