package uniGoettingen.DPI.ism;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.WritableRaster;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;


import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.io.Opener;
import ij.process.FloatProcessor;
import ij.process.ImageProcessor;
import ij.process.ImageStatistics;
import jdk.internal.util.xml.impl.Parser;

public class ISMprocessor {
	
	public int nProcessed;
	public float[] ISMimage= null;
	public float[] AVGimage= null;
	public float PixelSize = 1;
	boolean finished = false;	
	float nPSFpixel = 9;
	float FWHM = 400;
	
	int scale = 1;
	int Wavg = 0;
	int Havg = 0;
	int Wism = 0;
	int Hism = 0;
	int Wimg = 0;
	int Himg = 0;
	int background = 0;
	ISMImageProcessorGUI ismipGUI = null;
	String savePath = "E:/GWDG/ISM/data/superimg/superimage.png";
	String RefDir = "";
	String ImageDir = "";
	private String ImageName = "";
	boolean isBackgroundset = false;
	String ProcessAlg = "";
	
	int nFrames = 250;
	int nSlice = 1;
	
	String ImagFileName = "";
	List<String> sampleFlist = null;
	ImagePlus imPlus = null;
	ImagePlus imDisp = null;
	
	public ISMprocessor(ISMImageProcessorGUI ismImageProcessorGUI) {
		// TODO Auto-generated constructor stub
		ismipGUI = ismImageProcessorGUI;
	}

	
	public float[] normaliz(float[] image){
		int len = image.length; 
		if(len<=0){
			return null;
		}
		
		float[] sorted = image.clone();
		Arrays.sort(sorted);
		
		float max = sorted[len-1];
		float min = sorted[0];
		float[] normalized = image.clone();
		
		if(max>min){
			
			
			if(sorted[len-1]<=sorted[0]){
				return null;
			}
			
			for(int m=0; m<len; m++){
				if(image[m]<=0){
					normalized[m]=0;
				}else{
					normalized[m] = (image[m]-min)/(max-min);
				}
				
			}
		}
		

		return normalized;
		
		
	}
	

	float[] getROI(float[] Img, int W, int H, int Wdisp, int Hdisp){
		

		int Wmid = W/2;
		int Hmid = H/2;
		int Nl = Wmid - Wdisp/2;
		int Nr = Nl + Wdisp;
		int Ml = Hmid - Hdisp/2;
		int Mr = Ml + Hdisp;
		
		float[] Image = new float[Wdisp*Hdisp];
		int k = 0;
		for(int i=Ml; i<Mr; i++)
		for(int j=Nl; j<Nr; j++){
			if(i>=0 && i<H && j>=0 && j<W){
				Image[k] = Img[i*W+j]; //shift image to center of display view
			}
			
			k++;
			
		}
		return Image;
	}
	
	
	public BufferedImage CreatBuferimage(short[] img, int W, int H){
		
    	BufferedImage bufferimg = new BufferedImage(W, H, BufferedImage.TYPE_USHORT_GRAY);
    	WritableRaster raster = bufferimg.getRaster();
    	//bufferimg.getData();
    	//DataBufferInt databuffer = (DataBufferInt) raster.getDataBuffer();
    	DataBuffer databuffer = raster.getDataBuffer();
    	for(int i=0;i<img.length;i++){
    		databuffer.setElem(i, img[i]);
    		
    	}
    	bufferimg.setData(raster);
    	return bufferimg;
	}
	
	
    public synchronized int SumImageup(int frameNum, float[] image, int Width, int Height) {
    	for (int i = 0; i<image.length; i++){ 
    		float pixelval = image[i];
    		ISMimage[i] =  ISMimage[i] + pixelval;
    		//if(pixelval>background){
    		//	ISMimage[i] =  ISMimage[i] + pixelval - background;
    		//}
    	}
    	    	
		nProcessed++;
		ismipGUI.progressBar.setValue(nProcessed);
		ismipGUI.lblNf.setText(""+nProcessed);
		System.out.println("Processed Frame number: " + nProcessed);
		
		//display frame-accumulated ISM image
		ImageProcessor Ip = new FloatProcessor(Width, Height);
		Ip.setPixels(ISMimage);
		int Wid = Width/scale;
		int Hei = Height/scale;
		Ip = Ip.resize(Wid, Hei);
		float[] pixels = (float[]) Ip.getPixels();
		BufferedImage bufImage = transf2BufferedImage(pixels, Ip.getWidth(), Ip.getHeight(), Wid, Hei);
		ismipGUI.bufImage = bufImage;
		ismipGUI.repaint();
		
		//display ISM of each frame
		int Wdisp = 1024; 
		int Hdisp = 800;
		if(frameNum==1){
			imDisp = IJ.createImage("ISM Each Frame", "32-bit black", Wdisp, Hdisp, 1);  
		}
		ImageProcessor ip = imDisp.getProcessor();
		ip.setPixels(getROI(image, Width, Height, Wdisp, Hdisp)); 
		IJ.run(imDisp, "Enhance Contrast", "saturated=0.5 normalize");
		imDisp.show();		
		
		//For testing
		File dir = new File(ImageDir + "//ISM_EACH_Frame//");		
		
		short[] img = new short[image.length];
		for(int i = 0; i<image.length; i++){
			int pixel = (int) (image[i]);
			
			if(pixel>background){
				img[i] = (short) ( pixel - background);
			}
		}		
				
		ImagePlus imp = IJ.createImage("My new image", "16-bit black", Width, Height, 1); 
		ip = imp.getProcessor();
		ip.setPixels(img);
		IJ.run(imp, "Enhance Contrast", "saturated=0.5 normalize");
	    BufferedImage buffImage = ip.getBufferedImage();
		if(dir.isDirectory() && dir.exists()){
		    try {
				ImageIO.write(buffImage, "png", new File(dir.getAbsolutePath()+"/frame"+frameNum+".png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}   
	    /*  */
        return 0;
    }
	
     
    public void saveImageAs(String path){
    	if(imPlus!=null){
    		//saveImage(AVGimage, Wavg, Havg, path);
    		/*
			ImageProcessor ip = imPlus.getProcessor();    		
    		ip = ip.convertToShortProcessor();
    		//ip = ip.convertToShort(false);
    		IJ.save(new ImagePlus("",ip), path);
    		
    		*/
    		int n = path.lastIndexOf(".");
    		if(n<1){
    			return;
    		}
    		
    		ImageStack imstack = imPlus.getStack();
    		int N = imstack.getSize();
    		
    		if(N==1){
    			
    			ImageProcessor ip = imPlus.getProcessor();    		
        		ip = ip.convertToShortProcessor();
        		//ip = ip.convertToShort(false);
        		IJ.save(new ImagePlus("",ip), path); 
        		//IJ.saveAs(new ImagePlus("",ip), "png", path);
        		return;
        		
    		}else if(N>1){
    			
        		for(int i=1; i<=N; i++){
        			ImageProcessor ip = imstack.getProcessor(i);    		
    	    		ip = ip.convertToShortProcessor();
    	    		//ip = ip.convertToShort(false);
    	    		String path1 = path.substring(0, n-1)+"_"+i+path.substring(n);
    	    		IJ.save(new ImagePlus("",ip), path1);
    	    		
        		}
    		}
    		


    	}
    	
    	
    }
    
    
     public synchronized void saveImage(float[] InputImage, int W, int H, String path ){

			float[] image = InputImage.clone();
			if(image.length>0){// nframe-1	
				
				ImagePlus imp = IJ.createImage("My new image", "32-bit black", W, H,  1);  
				ImageProcessor ip = imp.getProcessor();
				//ip = ip.convertToFloatProcessor();
				ip.setPixels(image);
				
				//IJ.run(imp, "Enhance Contrast", "saturated=0.05");//  normalize equalize histogram
				
				
				
				ip = imp.getProcessor();
				ip.setInterpolate(true);
				ip.setInterpolationMethod(ImageProcessor.BILINEAR);
				ip = ip.resize(2*Wimg, 2*Himg);
				
				ip.resetMinAndMax();
				
				//ip = imp.getProcessor();
				//ip.convertToFloat();
				//ip = ip.convertToShort(true);
				
				
				//imp.show();
				ImagePlus imp1 =  new ImagePlus("",ip);
				imPlus = imp1;
				//imp.show();
				
				//IJ.saveAs(imp, "tif", path);
				IJ.saveAsTiff(imp1,path);
				/*
			    try {
			    	BufferedImage bufferimg = new BufferedImage(W, H, BufferedImage.TYPE_USHORT_GRAY);
			    	WritableRaster raster = bufferimg.getRaster();
			    	//bufferimg.getData();
			    	//DataBufferInt databuffer = (DataBufferInt) raster.getDataBuffer();
			    	DataBuffer databuffer = raster.getDataBuffer();
			    	for(int i=0;i<image.length;i++){
			    		//int x = i%(2*W);
			    		//int y = i/(2*H);
			    		//raster.setDataElements(x, y, image[i]); // doesn't work
			    		databuffer.setElem(i, image[i]);
			    		
			    	}
			    	bufferimg.setData(raster);
					ImageIO.write(bufferimg, "png", new File(path));//
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			    //System.out.print("ISM imageing processing finished!");
			*/
			}
						
					
		}
    
	

     

	public BufferedImage transf2BufferedImage(float[] inputImage, int W, int H, int Wnew, int Hnew){
		
		int Wdisp = ismipGUI.WidBufImg; 
		int Hdisp = ismipGUI.HeiBufImg;
		float[] Img = inputImage.clone();
		//float[] normalized = normaliz(Img);
		ImagePlus imp = IJ.createImage("My new image", "32-bit black", W, H,  1);  
		
		ImageProcessor ip = imp.getProcessor();
		ip.setPixels(inputImage);	
		
		//ip.setInterpolationMethod(ImageProcessor.BICUBIC); 
		//IJ.run(imp, "Enhance Contrast", "normalize");
		IJ.run(imp, "Enhance Contrast", "saturated=0.05"); 
		
		ip = imp.getProcessor();
		
		//imp.show();
		ip = ip.convertToShort(true);
		
		
		//imp.show();
		ImagePlus imp1 =  new ImagePlus("",ip);
		//imp1.show();
		ip = imp1.getProcessor();
		
		short[] Image;
		/*
		if(Wdisp!=W || Hdisp!=H){
			ip.setInterpolate(true);
			ip.setInterpolationMethod(ImageProcessor.BICUBIC);
			ImageProcessor ipl = ip.resize(Wdisp, Hdisp);
			
			//ImagePlus dispimage = new ImagePlus("New Image", ipl);
			//IJ.run(dispimage, "Enhance Contrast", "normalize");//saturated=0.1    equalize histogram	
			Image = (short[]) ipl.getPixels();
		}else{
			//IJ.run(imp, "Enhance Contrast", "normalize");//saturated=0.1     equalize histogram
			Wdisp = W;
			Hdisp = H;
			Image = (short[]) ip.getPixels();
			
		}
		
		*/
		
		
		short[] Pixel = (short[]) ip.getPixels();
		int Wmid = W/2;
		int Hmid = H/2;
		int Nl = Wmid - Wdisp/2;
		int Nr = Nl + Wdisp;
		int Ml = Hmid - Hdisp/2;
		int Mr = Ml + Hdisp;
		
		Image = new short[Wdisp*Hdisp];
		int k = 0;
		for(int i=Ml; i<Mr; i++)
		for(int j=Nl; j<Nr; j++){
			if(i>=0 && i<H && j>=0 && j<W){
				Image[k] = (short) Pixel[i*W+j]; //shift image to center of display view
			}
			
			k++;
			
		}
		
		//dispimage.show();
		
		BufferedImage bufferimg = new BufferedImage(Wdisp, Hdisp, BufferedImage.TYPE_USHORT_GRAY);
		WritableRaster raster = bufferimg.getRaster();
		DataBuffer databuffer = raster.getDataBuffer();
		for(int i=0;i<Image.length;i++){
			short pixel = Image[i];			
			databuffer.setElem(i, pixel);    		
		}
		bufferimg.setData(raster);
		return bufferimg;
	}
	
	public BufferedImage getAverage(String path){
		
		Opener opener = new Opener();
		
		ismipGUI.lbl_ProgMessage.setText("Loading image, please wait...");
		ImageStack imstack = opener.openImage(path).getStack();
		int Nframe = imstack.getSize();
		int Wa = imstack.getWidth();
		int Ha = imstack.getHeight();
		
		Wimg = Wa;
		Himg = Ha;		
		
		Wa = 2*Wa;
		Ha = 2*Ha;
		Wavg = Wa;
		Havg = Ha;
		
		ismipGUI.lbl_ProgMessage.setText("Calculating average...");
		
		int len = Wa*Ha;
		if(Nframe<1 || len<1){
			return null;
		}
		
		if(nSlice == 1){
			
			
			if(nFrames*nSlice!=Nframe){
				
				JOptionPane.showMessageDialog(ismipGUI, "Error: Slice Number might not correct!");
				return null;
			}
			
			ismipGUI.progressBar.setMaximum(Nframe);
			float [] aveImage = new float[len];
			float[] SumImage= new float[len];
			ImageProcessor ip;
			for(int i=1; i<=Nframe; i++){
				
				if(ismipGUI.isWindowClosed || ismipGUI.isStop){
					return null; // stop thread when Jframe is colsed
				}
				
				ip = imstack.getProcessor(i);	
				ip.setInterpolationMethod(ImageProcessor.BICUBIC);
				ip = ip.resize(Wa, Ha);
				ip = ip.convertToFloatProcessor();
				float[] pixel = (float[]) ip.getPixels();
				for(int j=0; j<pixel.length; j++){				
					SumImage[j] = SumImage[j] + pixel[j];
				}	
				ismipGUI.progressBar.setValue(i);
				ismipGUI.lblNf.setText(""+i);
				//aveImage = SumImage;
				//break;
			}
			
			for(int j=0; j<SumImage.length; j++){				
				aveImage[j] = (SumImage[j]/(Nframe));
			}	
			
			//AVEimage = aveImage.clone();
			ProcessAlg = "AVG";
			AVGimage = aveImage;
			
			String AVGpath= ImageDir + "\\AVG_" + ImageName;
			
			saveImage(aveImage, Wa, Ha, AVGpath);
		
			ismipGUI.lbl_ProgMessage.setText("Done!");
			//return transf2BufferedImage(aveImage, Wa, Ha, Wa, Ha);
			BufferedImage bufImage = transf2BufferedImage(aveImage, Wa, Ha, Wa, Ha);
			ismipGUI.bufImage = bufImage;
			ismipGUI.repaint();
			
		}else{
			
			getAverageForZslice(imstack, path);
			
		}
		return null;
		
	}
		
	
	
	public float[] ISMreconCML(int frameNum, double[] X, double[] Y, int len, float[] pixel, int Width, int Height, int scales, float Npixel){
			
			
			double[] cx = X; 
			double[] cy = Y; 
			int length = len;
			int W = Width;
			int H = Height;
			int Ns = scales;		  	
	    	
	    	float[] superImage = new float[4*pixel.length];
	    	int x, y;
	    	int Nr = (int) (Ns*Npixel/2);
	    	int m, n;
	    	//r = 7;
	    	//r = 13;
	    	for(int i = 0; i<length; i++){
				
				//y = (int) Math.round((cy[i]-0.5)*Ns-0.5);  // Y'=Y*Scale-(Scale+1)/2
		    	//x = (int) Math.round((cx[i]-0.5)*Ns-0.5);  // X'=X*Scale-(Scale+1)/2
		    	y = (int) cy[i];
		    	x =	(int) cx[i];		
		    	
				int N1 = x - Nr;
		    	int M1 = y - Nr;
		    	
				int N2 = x + Nr;
		    	int M2 = y + Nr;
		    	
		    	
		    	int Ws = (N2 - N1 +1); // width of sub-image
		    	int Hs = (M2 - M1 +1); // height of sub image
				//copy subimage from low resolution image
		    	
		    	float[] subImage = new float[Ws*Hs];
		    	int j = 0;
		    	for(m = M1; m<=M2; m++)
		    		for(n = N1; n<=N2; n++){
		    			
		    			//System.out.println(m*w+n+"|"+ pixel.length + "|"+j+"|" + subImage.length);
		    			if(m >= 0 && n>=0  && m<H && n<W  && (m-y)*(m-y)+(n-x)*(n-x)<=Nr*Nr){
		    				subImage[j] = pixel[m*W+n];
		    			}else{
		    				subImage[j] = 0;
		    					
		    			}
		    			
		    			j++;
		    			
		    		}
		    	/*	
		    	ImagePlus imp = IJ.createImage("My new image", "16-bit black", wr+1, wr+1,  1);  
				ImageProcessor ip = imp.getProcessor();
				ip.setPixels(subImage);
				IJ.run(imp, "Enhance Contrast", "saturated=0.5 normalize");
				imp.show();*/
		    	
		    	
		    	// paste to super resolution image pixel grid
		    	
		    	
				//y = (int) Math.round((cy[i]-0.5)*2-0.5);  // Y'=Y*Scale-(Scale+1)/2
		    	//x = (int) Math.round((cx[i]-0.5)*2-0.5);  // X'=X*Scale-(Scale+1)/2
		    	
		    	y = (int) cy[i]*2;
		    	x = (int) cx[i]*2;
		    	
				N1 = x - Nr;
		    	M1 = y - Nr;
		    	
				N2 = x + Nr;
		    	M2 = y + Nr;
		    	
		    	
		    	j = 0;
		    	
		    	for(m = M1; m<=M2; m++)
		    		for(n = N1; n<=N2; n++){
		    			//System.out.println(m +"|"+ n);
		    			if(m >= 0 && n>=0  && m<2*H && n<2*W ){//&& (m-y)*(m-y)+(n-x)*(n-x) <= Nr*Nr
		    				superImage[m*(2*W)+n] = subImage[j]; //
		    				//System.out.println(2*m*w+n+"|"+ superImage.length + "|"+j+"|" + subImage.length);
		    			}
		    			j++;
		    	}
		    	
		
					
			}
	    	
	    	SumImageup(frameNum, superImage, 2*W, 2*H);				
	    	return superImage;
	
	}
	
	
	
	public float[] ISMstackProcCML(final ImageStack imstack, String txtfiledir){	
		
		  ISMimage =  null;
		  
		  BufferedReader bufferedInputReader = null;
					
		  try {		
			    
				FileInputStream fileInputStream = new FileInputStream(txtfiledir);
				bufferedInputReader = new BufferedReader(new InputStreamReader(fileInputStream));
	
				
				
				String lineFromFile = bufferedInputReader.readLine();	
				
				
				int scaleRe = scale;	
				double PixsizeX = 1, PixsizeY = 1; 
				double offset = 0;
				String saparator = " ";
				int xId = 0;
				int yId = 1;
				int framId = 2;
				
				
				if(lineFromFile.startsWith("# <localizations")){
					/*
					int PX = lineFromFile.indexOf("position in sample space in x dimension");
					int PY = lineFromFile.indexOf("position in sample space in y dimension");
					
					String strResol = lineFromFile.substring(PX, PY);			
					String strResolX = strResol.substring(strResol.indexOf("resolution="), strResol.indexOf("px"));
					strResolX = strResolX.replace("resolution=", "");
					strResolX = strResolX.replace("\"", "");
					double ResolX = Double.parseDouble(strResolX);
					
					strResol = lineFromFile.substring(PY);
					String strResolY = strResol.substring(strResol.indexOf("resolution="), strResol.indexOf("px"));
					strResolY = strResolY.replace("resolution=", "");
					strResolY = strResolY.replace("\"", "");
					double ResolY = Double.parseDouble(strResolY);
					
					
					PixsizeX = 1e9/ResolX; //in nanometer
					PixsizeY = 1e9/ResolY; //in nanometer
					*/
					offset = 0.5;
					
				} else if(lineFromFile.startsWith("TM_Localization")){
					
					//int PX = lineFromFile.indexOf("Scale=");
					//String strUpScale = lineFromFile.substring(PX+6);
					//if(strUpScale!=null){
					//	 scaleRe  = Integer.parseInt(strUpScale);
					//}
					
					
				}else if(lineFromFile.startsWith("GF_Localization")){					
						 //scaleRe  = scale;
					offset = -0.5;
					
				}else if(txtfiledir.endsWith(".csv")){
					saparator = ",";
					
					if(lineFromFile.indexOf("\"id\"")<0){ 
						framId = 0;
						xId = 1;
						yId = 2;
						
					}else{//include column of "id" in data
						framId = 1;
						xId = 2;
						yId = 3;
					}

					
				}
				
				double x[] = new double[100000];
				double y[] = new double[100000];		
				double [] xp = null;
				double[] yp = null;				
				
					
				int i = 0;
				int len = 0;
				int nfram0 = -1;
				int nfram1 = -1;
				PixsizeX = PixelSize;
				PixsizeY = PixelSize;
				
				
				//String filename = "E:/GWDG/ISM/data/170425 cells/reference_1/reference_1_MMStack_Pos0.ome.tif";
				//String filename = "E:/GWDG/ISM/data/640_reference_251_M1_1_M2_3_44cycles_4pulse/640_reference_251_M1_1_M2_3_44cycles_4pulse_MMStack_Pos0.ome.tif";
				//String filename = "E:/GWDG/ISM/data/IHCs_p14_calretinin488_Vglut568_Otof647/blue_1/blue_1_MMStack_Pos0.ome.tif"; //green_2/green_2_MMStack_Pos0.ome.tif"; //blue_1/blue_1_MMStack_Pos0.ome.tif";
				
				//imp.g
				//Opener opener = new Opener(); 
				//ImagePlus imp = opener.openImage(filename);
						 
				int width = imstack.getWidth();
				int height = imstack.getHeight();
				int Nframe = imstack.getSize();
				//int scale = 4;
				final int W = scaleRe*width;
				final int H = scaleRe*height;
				ismipGUI.progressBar.setMaximum(Nframe);
				
				
				Wism = 2*W;
				Hism = 2*H;
				//Wism = 4*W;
				//Hism = 4*H;
				ISMimage =  new float[Wism*Hism];
				
				nProcessed = 0;
				String[] lineResult=null;	
				int firstFrameId = 0;

			
				while (true){	
					
					if(ismipGUI.isWindowClosed || ismipGUI.isStop){
						return null; // stop thread when Jframe is colsed
					}
					
					lineFromFile = bufferedInputReader.readLine();
					boolean isNext = false;
					boolean isEOF = lineFromFile==null;
					
					if(isEOF){	
						
						len = i;
						if(len<=0){
							
							break;
							
						}
							
						xp = x.clone();
						yp = y.clone();							
					 
					}else{

						String[] line = lineFromFile.split(saparator);
						
						lineResult = new String[line.length];
						int k = 0;
						for(int j = 0; j<line.length; j++){
							if(!line[j].isEmpty()){
								lineResult[k] = line[j];
								k = k + 1;						
							}	
						}
						
						
					    try {
					    	Double.parseDouble(lineResult[xId]);
					    	Double.parseDouble(lineResult[yId]);
					    	Double.parseDouble(lineResult[framId]);					        
					    } catch (Exception e) {
					        continue;
					    }
						
						nfram0 = nfram1;
						nfram1  = (int) Double.parseDouble(lineResult[framId]);
						
						if(nfram1==0){// Frame index of thunderSTORM start from 1
							firstFrameId = 1;  
						}
						
						
						if(nfram1!= nfram0 && nfram0>=0){
							xp = x.clone();
							yp = y.clone();
							len = i; 
							i = 0;                     
							isNext = true;
						}	
					}
					// new frame
					if(isNext || isEOF){  // get all position data of one frame

						//System.out.println("Current Frame number: " +nfram0);
						System.out.println("nProcessed = "+nProcessed+"|"+Nframe+"|"+nfram0);

						final int nframe  = nfram0+firstFrameId;
						final int Len = len;
						//Thread thread = new Thread(){ // do single frame reconstruction by a thread
						  //  public void run(){
								if(nframe>250){
									
									//System.out.println(""+nfram0+"  " + nfram1);
									
									
								}
						    	ImageProcessor ip = imstack.getProcessor(nframe); // specify number of slice 			
								if(!isBackgroundset){
									//background = (int) ip.getStatistics().dmode;
									background = 0;
								}						
								
								ip = ip.convertToFloatProcessor();
								
								ip.setInterpolationMethod(ImageProcessor.BILINEAR);
								if(W!=Wimg && H!=Himg){
									ip = ip.resize(W, H);
								}
							    
								float[] pixel = (float[]) ip.getPixels();							

								int Wre = W;
								int Hre = H;
								
								float Np = nPSFpixel;

								
								ISMreconCML(nframe, xp, yp, Len, pixel, Wre, Hre, scaleRe, Np);
								
								if(isEOF){
									break; //finish and break here
								}	
								 
					}
					
					if(!isEOF){
						
						//x[i] = Double.parseDouble(lineResult[0])*scale;//-(scale+1)/2
						//y[i] = Double.parseDouble(lineResult[1])*scale;//-(scale+1)/2
						
						
						x[i] = (Double.parseDouble(lineResult[xId]))*scaleRe/PixsizeX+offset;
						y[i] = (Double.parseDouble(lineResult[yId]))*scaleRe/PixsizeY+offset;
						
						
						i++;
						
					}
					
				}
					
					
				
				
				//short[] superimg = new short[ISMimage.length];
				//for(i = 0; i<ISMimage.length;i++){
				//	superimg[i] = (short) (ISMimage[i] );//& 0xFFFF
				//}
				
				//saveImage(superimg, 2*W, 2*H, savePath);
				
				fileInputStream.close();
				bufferedInputReader.close();
				//ismipGUI.progressBar.setValue(nframe);
				imDisp.close();
				
				return ISMimage;
				//return normaliz(ISMimage);
				
			} catch (Exception e) {
				e.printStackTrace();
			
			
			}finally{
				
				try {
					if(bufferedInputReader!=null){
						bufferedInputReader.close();
					}
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		  
		  return null;
		 
	}
	
	
	
	
	
	
	
	
	
public ImageStack getAverageForZslice(ImageStack imstack, String imgpath){
		
		//Opener opener = new Opener();
		//ImageStack imstack = opener.openImage(path).getStack();
		int Nframe = imstack.getSize();
		int Wa = imstack.getWidth();
		int Ha = imstack.getHeight();
		int L = imstack.getSize();
		
		int TotalFrames = nFrames*nSlice; //1033+1033+946;
		//int nFrames = 251;
		
		
		Wa = 2*Wa;
		Ha = 2*Ha;
		int len = Wa*Ha;
		if(Nframe<1 || len<1){
			return null;
		}
		
		int frames = nFrames; //number of frame each slice		
		
		if(nFrames*nSlice<Nframe){
			
			JOptionPane.showMessageDialog(ismipGUI, "Error: Slice Number might not correct!");
			return null;
		}
		
		ImageStack avgStack = null;
		int k = 1;
		int L0 = 0;
		int nstack = 1;
		float minPixel = 0xFF, maxPixel = 0;
		ismipGUI.progressBar.setMaximum(nSlice);
		ismipGUI.lbl_Slice.setText("NSlice: "+1);
		
		ismipGUI.lbl_ProgMessage.setText("Calculating average image...");
		
		for(int i=1; i<=TotalFrames; i=i+nFrames){
			
			float[] SumImage= new float[len];
			for(int j=i; j<i+nFrames; j++){

				if(ismipGUI.isWindowClosed || ismipGUI.isStop){
					return null; // stop thread when Jframe is colsed
				}
				
				if(j>L+L0){
					for(int p = 1; p<=L; p++){
						//System.out.println("P = "+ p);
						imstack.deleteLastSlice();
					}
					imstack = null;
					IJ.freeMemory();
					System.gc();
					//System.out.println("Max Memory = "+ IJ.maxMemory());
					String stackpath = imgpath.replaceAll(".ome.tif", "_"+nstack+".ome.tif");
					
					Opener opener = new Opener();
					ismipGUI.lbl_ProgMessage.setText("Loading raw image...");
					imstack = opener.openImage(stackpath).getStack();
					ismipGUI.lbl_ProgMessage.setText("");
					L0 = L0 + L;
					L = imstack.getSize();
					nstack++;
				}
				System.out.println("Processing Frame Number:"+(j - L0) + " "+(j-i+1));
				if(j-L0 == 86){
					int q=j;
					q++;
				}
				
				ImageProcessor ip = imstack.getProcessor(j - L0);					
				
				//ImageProcessor ip = imstack.getProcessor(j);	
				ip.setInterpolationMethod(ImageProcessor.BICUBIC);
				ip = ip.resize(Wa, Ha);
				ip = ip.convertToFloatProcessor();
				float[] pixel = (float[]) ip.getPixels();
				
				for(int p=0; p<pixel.length; p++){				
					SumImage[p] = SumImage[p] + pixel[p];
				}	
				
				ismipGUI.lblNf.setText(""+(j-i+1));
			}
			
			//calcuate average
			for(int p=0; p<SumImage.length; p++){				
				SumImage[p] = SumImage[p]/frames;
			}
			
			
			if(avgStack==null){
				//ismStack = new ImageStack(Wism, Hism, nSlice);
				avgStack = new ImageStack(Wa, Ha, nSlice);
			}
			
			ismipGUI.progressBar.setValue(k);
			ismipGUI.lbl_Slice.setText("NSlice: "+k);
			//add average image to image stack
			//avgStack.setPixels(SumImage, k);
			
			
			avgStack.setPixels(SumImage, k);
			ImageProcessor ip = avgStack.getProcessor(k);
			
			
			double pix1 = ip.getMax();
			double pix2 = ip.getMin();
			
			
			if(k==1){
				maxPixel = (float) pix1;
				minPixel = (float) pix2;
			}else{
				if(pix1>maxPixel){
					maxPixel = (float) pix1;
				}
				if(pix2<minPixel){
					minPixel = (float) pix2;
				}
			}
			
			//ip = ip.convertToShort(false);
			//ImagePlus improcessor = new ImagePlus("",ip);
			//IJ.run(improcessor, "Enhance Contrast", "saturated=0.05");//  normalize equalize histogram
			//improcessor.show();
			avgStack.setProcessor(ip, k);
			
			BufferedImage bufImage = transf2BufferedImage(SumImage, Wa, Ha, Wa, Ha);
			ismipGUI.bufImage = bufImage;
			ismipGUI.repaint();
			
			
			k++;
		}
		

		
		ImagePlus imp = new ImagePlus("",avgStack);
		//IJ.run(imp, "Enhance Contrast", "saturated=0.05");//  normalize equalize histogram
		//imp.show();
		imp.setDisplayRange(minPixel, maxPixel);
		imPlus = imp;
		IJ.saveAsTiff(imp, ImageDir+"\\"+ "AVG_Z-Stack" + ImageName);
		System.out.println("average image stack is saved!");
		ismipGUI.lbl_ProgMessage.setText("Done!");
		return null;
		
	}
	


   public float[] SingleSlicISMrecon(ImageStack imstack, String refpath){
    	//search corresponding txt file of localized data
        String txtPath = "";
        
        if(refpath.endsWith(".tif") || refpath.endsWith(".tiff")){
			int ind = refpath.lastIndexOf(".");
			if(ind>=1){
				txtPath = refpath.substring(0, ind)+".Localiz.txt";
			}else{
				return null;
			}
        	//txtPath = refpath.substring(0, refpath.lastIndexOf(".tif")) + ".POS_XY.txt";
        	File txtFile = new File(txtPath);
        	
            // If file doesn't exists, do localization
            if(!txtFile.exists()) {
				imStackLocalize(refpath);
			}
            
        }else if(refpath.endsWith(".txt") || refpath.endsWith(".csv") ){
        	
        		 txtPath = refpath;
        	
              }else{
	            	JOptionPane.showMessageDialog(ismipGUI, "Input reference must be txt, csv or tif/tiff file!");
	            	return null;
              }
		
		long start = System.currentTimeMillis();		
		//calculate ISM image with position data of each scanning point
		
		ismipGUI.lbl_ProgMessage.setText("");
		//float[] IsmImage = ISMstackProcRapidSTORM(imstack,refpath);
		float[] IsmImage = ISMstackProcCML(imstack,txtPath);	
		
		long end = System.currentTimeMillis();
		long costtime = end - start;
		System.out.println("Reconstruction time ="+costtime/1000+"s");
		ismipGUI.lbl_ProgMessage.setText("Reconstruction done!");

    	return IsmImage;
    }

   
   public void MulISMimageProcess(String refpath){
	   
	   //check the input file firstly
	   if(sampleFlist!=null){
		   for(int i=0; i<sampleFlist.size(); i++){
	   		   String imgpath = sampleFlist.get(i);
	   		   
	   		   if(imgpath==null){
	   			   continue;
	   		   }
	   		   
	   		   File imFilePath =  new File(imgpath);
	   		   ImageDir = imFilePath.getParent();
	   		   ImageName  = imFilePath.getName();
			   if(!imFilePath.exists()){ 
					
					 JOptionPane.showMessageDialog(ismipGUI, "Raw image file not found: " + imFilePath);
					 return;
			   }
			   
			   
			   if(!imgpath.endsWith(".tif") && !imgpath.endsWith(".tiff") ){
				   JOptionPane.showMessageDialog(ismipGUI, "Input sample raw image file must be tif/tiff file!");
				   return;
			   }
		   }
	   }
	   
	   // implement raw image stacks processing
	   if(sampleFlist!=null){
		   for(int i=0; i<sampleFlist.size(); i++){
	   		   String imgpath = sampleFlist.get(i);
	   		   
	   		   if(imgpath==null){
	   			   continue;
	   		   }
	   		   
	   		   File imFilePath =  new File(imgpath);
	   		   ImageDir = imFilePath.getParent();
	   		   ImageName  = imFilePath.getName();
			   if(!imFilePath.exists()){ 
					
					 //JOptionPane.showMessageDialog(ismipGUI, "Raw image file not found: " + imFilePath);
					 continue;
			   }
			   
			   
			   if(!imgpath.endsWith(".tif") && !imgpath.endsWith(".tiff") ){
				   //JOptionPane.showMessageDialog(ismipGUI, "Input sample raw image file must be tif/tiff file!");
				   continue;
			   }
	   		   
	   		   if(ProcessAlg.equals("ISM")){
	   			   
					if(!new File(refpath).exists()){ 
						
						 JOptionPane.showMessageDialog(ismipGUI, "Reference file not found: " + refpath);
						 return;
					}
	   			   ISMimageProcess(imgpath, refpath);
	   		   }else if(ProcessAlg.equals("AVG")){
	   			   getAverage(imgpath);
	   		   }
	   		   
		   }

	   }
   }
   
   
   
	public BufferedImage ISMimageProcess(String imgpath, String refpath){

		Opener opener = new Opener();
		
		ismipGUI.lbl_ProgMessage.setText("Loading image, please wait...");
		ImageStack imstack = opener.openImage(imgpath).getStack();
		
		Path path = Paths.get(imgpath);
		ImagFileName = path.getFileName().toString();
		
		
		//if intput file is txt, no need to calculate position
		int W = imstack.getWidth();
		int H = imstack.getHeight();
		int L = imstack.getSize();
		int TotalFrames = nFrames*nSlice;
		
		
		Wimg = W;
		Himg = H;
		

		if(background>=0){
			isBackgroundset = true;
		}else{
			isBackgroundset = false;
			background = 0;
		}
			
		
		ProcessAlg = "ISM";
		
		ismipGUI.lbl_ProgMessage.setText("Start ISM image calculation...");
		IJ.wait(300);
		ismipGUI.lbl_ProgMessage.setText("");
		
		if(nSlice == 1){
			ismipGUI.lbl_Slice.setText("NSlice: "+1);
			if(nFrames*nSlice!=L){
				
				JOptionPane.showMessageDialog(ismipGUI, "Error: Slice or frame Number might not correct!");
				return null;
			}
			
			
			float[] IsmImage = SingleSlicISMrecon(imstack, refpath);
			String ISMpath= ImageDir + "\\ISM_" + ImageName;
			
			saveImage(IsmImage, Wism, Hism, ISMpath);
			
			ismipGUI.lbl_ProgMessage.setText("ISM image has been saved!");
			IJ.wait(300);
			ismipGUI.lbl_ProgMessage.setText("Single-Slice ISM reconstruction Done!");
			//ImageProcessor ip = imPlus.getProcessor();
			//BufferedImage bufImage = transf2BufferedImage((float[])ip.getPixels(), ip.getWidth(), ip.getHeight(), 2*W, 2*H);
			//ismipGUI.bufImage = bufImage;
			//ismipGUI.repaint();
			return null;
		
			
		}else{
			
			
			if(nFrames*nSlice<L){
				
				JOptionPane.showMessageDialog(ismipGUI, "Error: Slice or frame Number might not correct!");
				return null;
			}
			
			int k = 1;
			int L0 = 0;
			int nstack = 1;
			
			//ismStack = new ImageStack(Wism, Hism, nSlice);
			ImageStack ISMimStack = new ImageStack(2*W,2*H, nSlice);	
			float minPixel = 0, maxPixel = 0xFF;
			ismipGUI.lbl_ProgMessage.setText("Calculating ISM image...");
			ismipGUI.lbl_Slice.setText("NSlice: "+1);
			for(int i=1; i<=TotalFrames; i=i+nFrames){
				
				ImageStack SliceImstack = new ImageStack(W, H, nFrames); //get imstack of one slice
				for(int j=i; j<i+nFrames; j++){
										
					if(ismipGUI.isWindowClosed || ismipGUI.isStop){
						return null; // stop thread when Jframe is colsed
					}	
					
					if(j>L+L0){
						for(int p = 1; p<=L; p++){
							//System.out.println("P = "+ p);
							imstack.deleteLastSlice();  //delete the processed imstack to release volume of memory
						}
						imstack = null;
						IJ.freeMemory();
						System.gc();
						//System.out.println("Max Memory = "+ IJ.maxMemory());
						String stackpath = imgpath.replaceAll(".ome.tif", "_"+nstack+".ome.tif");
						//Opener Next_Opener = new Opener();
						ismipGUI.lbl_ProgMessage.setText("Loading raw image...");
						imstack = opener.openImage(stackpath).getStack();
						ismipGUI.lbl_ProgMessage.setText("");
						W = imstack.getWidth();
						H = imstack.getHeight();
						L0 = L0 + L;
						L = imstack.getSize();
						nstack++;  //number of imstack file
					}
					
					ImageProcessor ip = imstack.getProcessor(j - L0);	
					//ImageProcessor ip = imstack.getProcessor(j);	

					//imstack_.addSlice(ip);							
					//imstack_.setProcessor(ip, k);
					int n = j-i+1;
					SliceImstack.setPixels(ip.getPixels(), n);
					//ip = SliceImstack.getProcessor(n);
					//ImagePlus imp1 = new ImagePlus("",ip);
					
					//System.out.println("Processing Frame:"+(j - L0)+" "+n);
					//imp1.show();
				}
				
				//calculate ISM image of one slice
				float[] IsmImage = SingleSlicISMrecon(SliceImstack, refpath);
			
				// push single-slice ISM image to ISMimStack
				ImagePlus imp = IJ.createImage("My new image", "32-bit black", Wism, Hism,  1);  
				ImageProcessor ip = imp.getProcessor();
				//int Len = ISMimage.length;
				//float[] ISMimg = new float[Len];
				//for(int p=0; p<Len; p++){
					//ISMimg[p] = (float) ISMimage[p];
				//}
				ip.setPixels(ISMimage);	
				
				//ip.resetMinAndMax();
				//imp.show();
				//ip = imp.getProcessor();
				
				if((Wism*Hism)!=(4*W*H)){
					ip.setInterpolate(true); 
					//ip.setInterpolationMethod(ImageProcessor.BICUBIC); 
					ip = ip.resize(2*W,2*H);
				}
				
				
				//IJ.run(imp, "Enhance Contrast", "saturated=0.05");//  normalize equalize histogram
				//ip = imp.getProcessor();
				//ip = ip.convertToShort(false);
				
				//imp.show();
				//ismStack.setPixels(ip.getPixels(), k);
				
				ISMimStack.setPixels(ip.getPixels(), k);
				ip = ISMimStack.getProcessor(k);
				
				float pix1 = (float) ip.getMax();
				//float pix2 = (float) ip.getMin();
				
				float[] Pixels = (float[]) ip.getPixels();
				
				float pix2 = pix1;
				for(int q=0; q<Pixels.length; q++){
					if(Pixels[q]<pix2 && Pixels[q]>0){
						pix2 = Pixels[q];
					}
				}
				
				
				if(k==1){
					maxPixel = pix1;
					minPixel = pix2;
				}else{
					if(pix1>maxPixel){
						maxPixel = pix1;
					}
					if(pix2<minPixel){
						minPixel = pix2;
					}
				}
				

				//for testing
				//ImagePlus imp_test = new ImagePlus("",ip);
				//imp1.show();
				//IJ.saveAs(imp_test, "tiff", "D:/GWDG/ISM/data/superimg/superimage_"+k+".png");
				System.out.print("Number of Processed Slice:"+k);	
				
				ismipGUI.lbl_Slice.setText("NSlice: "+k);
				
				
				//BufferedImage bufImage = transf2BufferedImage(Pixels, 2*W, 2*H, 2*W, 2*H);
				//ismipGUI.bufImage = bufImage;
				//ismipGUI.repaint();
				
				k++;
			}
			
			
			ImagePlus imp = new ImagePlus("",ISMimStack);
			imp.setDisplayRange(minPixel, maxPixel);
			imPlus = imp;
			//IJ.run(imp, "Enhance Contrast...", "saturated=0.1"); 
			//imp.setStack(ismStack);
			//imp.show();
			String ISMpath= ImageDir + "\\ISM_Z-Stack" + ImageName;
			IJ.saveAsTiff(imp, ISMpath);
			System.out.println("ISM image stack is saved!");
			
			ismipGUI.lbl_ProgMessage.setText("ISM image has been saved!");
			IJ.wait(300);
			ismipGUI.lbl_ProgMessage.setText("Z-stack ISM reconstruction Done!");
			
			return null;
		}
		
		
	}
	
	
	void imStackLocalize(String RefPath){
		//float[] IsmImage = ISMstackProc(imstack, null, 0.6);
		long start = System.currentTimeMillis();
		ISMlocalizor.imStackLocalizer1(RefPath, scale, Math.round(nPSFpixel), background, ismipGUI);
		//ISMlocalizor.imStackLocalizerMultiThread(RefPath, scale, nPSFpixel, ismipGUI);
		
		long end = System.currentTimeMillis();
		long costtime =   end-start;
		System.out.println("Localization time ="+costtime/1000+ "s");
		
	}
	
	 public static void main(String[] args) throws IOException{
			
		 //String filedir = "C:/Users/Administrator/Desktop/xy.txt";
		String filedir = "E:/GWDG/ISM/data/pos1.txt";//locationxy.txt";
		String[] a = filedir.split(";");
		for(int i=0; i<=251; i++){
			
		}
		Path p = Paths.get("/ab/cd/xyz.txt");
		String fileName = p.getFileName().toString();
	    String directory = p.getParent().toString();
		//System.out.println(i%250);
		//load image stack
		//String filename = "E:/GWDG/ISM/data/IHCs_p14_calretinin488_Vglut568_Otof647/blue_1/blue_1_MMStack_Pos0.ome.tif"; //green_2/green_2_MMStack_Pos0.ome.tif"; //blue_1/blue_1_MMStack_Pos0.ome.tif";
		
		String refdir = "E:/GWDG/ISM/data/640_reference_251_M1_1_M2_3_46cycles_4pulse/640_reference_251_M1_1_M2_3_46cycles_4pulse_MMStack_Pos0.ome.tif";
		//String imgdir = "E:/GWDG/ISM/data/green_8/green_8_MMStack_Pos0.ome.tif";
		
		//D:\GWDG\ISM\data\170718 Robert cells\reference_640_125_ORCA_full\reference_640_125_ORCA_full_MMStack_Pos0.ome.tif
		String imgdir = "U:/Sebastian/Data/170728 ISM ORCA/cell_405nm_416LP_zStack_1/cell_405nm_416LP_zStack_1_MMStack_Pos0.ome.tif";
		
		
		String txtFile = "U:/Sebastian/Data/180223 ISM/ref405_Chroma_30c_250im_4p_25d_1/ref405_Chroma_30c_250im_4p_25d_1_MMStack_Pos0.ome.txt";
		
		String imPath = "U:\\Sebastian\\Data\\180426 ISM\\180427 ISM cell\\vero3_zStack_405_250im_4p_30c_42d_1\\AVG_zSlices.tif";
		ImagePlus imp = new ImagePlus(imPath);
		
		//IJ.run(imp, "Enhance Contrast...", "saturated=0.1"); 
		float pmin = 0, pmax = 0xFF;
		ImageStack imstack = imp.getImageStack();
		for(int i=1; i<=imstack.size(); i++){
			ImageProcessor ip = imstack.getProcessor(i);
			
			FloatProcessor fp = new FloatProcessor(ip.getWidth(), ip.getHeight());
			fp.setPixels(ip.getPixels());
			float pix1 = (float) ip.getMin();
			float pix2 = (float) ip.getMax();
			
			//ip.setMinAndMax(0, 500);
			//float pix1 = (float) ip.getMin();
			//float pix2 = (float) ip.getMax();
			
			if(i==1){
				pmin = pix1;
				pmax = pix2;
			}else{
				if(pix1<pmin){
					pmin = pix1;
				}
				
				if(pix2>pmax){
					pmax = pix2;
				}
			}
			

			
		}

		imp.setDisplayRange(pmin,pmax);
		IJ.saveAsTiff(imp, "U:\\Sebastian\\Data\\180426 ISM\\180427 ISM cell\\vero3_zStack_405_250im_4p_30c_42d_1\\ssAVG_zSlices.tif");
		//imp.setDisplayRange(0,1, 8);
		imp.show();
		//Parser parser = new Parser(dxt);

	 }

}
