package uniGoettingen.DPI.ism;

import java.io.IOException;
import java.util.Arrays;

import Jama.Matrix;
import ij.ImagePlus;
import ij.process.ImageProcessor;

/*
*  This class is for accurate 2D Gaussian fitting to given data set
*   
*  Author: Shun Qin
*  Contact: shun.qin@phys.uni-goettingen.de
*  copyright reserved: Something should be cited, if this code is copied to use.
*/



public class Gaussianfitter {

	double[] Zn;
	double[] X;
	double[] Y;
	int M, N, L;
	double Xl, Xr, Yl, Yr;
	boolean[] Mask;
	int Lmask = 0;
	//(x0,y0) is the index of first pixel
	Gaussianfitter(double[] Data, int Width, int Height, double x0, double y0, boolean[] mask){
		
		if(Data==null){
			return;
		}
		
		L = Data.length;
		
		X = new double[L];
		Y = new double[L];
		//Mask = new boolean[L];
		
		
		M = Height;
		N = Width;
		
		
		Xl = x0;
		Xr = x0 + Width-1;
		Yl = y0;
		Yr = y0 + Height -1;
		
		//create coordinate
		for(int i=0; i<Height; i++)
		for(int j=0; j<Width; j++){
			X[i*Width+j] = x0 + j;
			Y[i*Width+j] = y0 + i;	
			
		}
		
		
	
		Zn = Data.clone();
	
		//select high SNR pixels
		Lmask = 0;
		if(mask!=null && mask.length==L){
			Mask = mask.clone();			
			for(int i=0; i<L; i++){
				if(Mask[i]){
					Lmask++;
				}
				
			}
			
		}

		
		//at least 5 pixels are required
		if(Lmask<5){
			Mask = new boolean[L];
			for(int i=0; i<L; i++){
				Mask[i] = true;
			}
			Lmask = L;
		}
		
		
	}
	
	double[] Gaussianfun(double[] p){
		
		double A = p[0];
		double a = p[1];
		double b = p[2];
		double sig = p[3];
		double C = p[4];
		
		double[] G = new double[L];
		for(int i=0; i<L; i++){
			G[i] = A*Math.exp(-(Math.pow(X[i]- a, 2) + Math.pow(Y[i] - b, 2))/(2*sig))+C;			
		}
		return G;
		
	}
	
	
	double objfun(Matrix P, double[] Zn){
		
		int N = P.getRowDimension();
		double [] p = new double[N];
		
		p=P.getRowPackedCopy();
		
		double[] G = Gaussianfun(p);
		L=G.length; 
		double e=0;
		double d;
		for(int i=0;i<L;i++){
			d = G[i] - Zn[i];
			if(Mask[i]){
				e += d*d;
			}
		}
		
		return e;	
	}
	
	
	double objfun(double[][] R){
		
		
		L=R.length; 
		double e=0;
		double d;
		for(int i=0;i<L;i++){
			d = R[i][0];
			if(Mask[i]){
				e += d*d;
			}
		}
		
		return e;	
	}
	
	void getDerivative(double[][] p, double X[], double Y[], double Zn[], double[][]J, double[][] R){
		
		double A = p[0][0];
		double a = p[1][0];
		double b = p[2][0];
		double sig = p[3][0];
		double C = p[4][0];
		
		int Lp = 5;
		int L = Zn.length;
		//Jacobian matrix
		//double[] J = new double[L*Lp];		
		//double[] R = new double[L];
		
		double[] S = new double[L];	
		double[] AS = new double[L];
		double[] Sq = new double[L];
		
		for(int i=0; i<L; i++){
			Sq[i] = (X[i] - a)*(X[i] - a) + (Y[i] - b)*(Y[i] - b);			
			S[i] = Math.exp(-Sq[i]/(2*sig));	
			AS[i] = A*S[i];
		}

		//R = (gi-zn)
		for(int i=0; i<L; i++){
			R[i][0] = AS[i]+C-Zn[i];
			//System.out.println(""+R[i][0]);
		}
		
		//dA == S
		for(int i=0; i<L; i++){
			J[i][0] = S[i];			
		}

		//da
		for(int i=0; i<L; i++){
			J[i][1] = AS[i]*(X[i]-a)/sig;			
		}
		//db
		for(int i=0; i<L; i++){
			J[i][2] = AS[i]*(Y[i]-b)/sig;			
		}
		//dsig
		for(int i=0; i<L; i++){			
			J[i][3] = AS[i]*Sq[i]/(2*sig*sig);		
		}
		//dc
		for(int i=0; i<L; i++){
			J[i][4] = 1;			
		}
	
		
	}
	

	
	double[] fitGN(double[] p0, int Nmax){
		
		int Lp = 5;
		double[][] T = new double[Lp][1];
		
	    for(int i=0; i<Lp; i++){
	    	T[i][0] = p0[i]; 
	    }
	    
		Matrix  P = new Matrix(T);
		Matrix  Pre = P.copy();
		//RealMatrix P = new Array2DRowRealMatrix(T);
		
		double[][] R = new double[L][1];
		double[][] J = new double[L][Lp];		
		double[] p1 = p0.clone();
		// iterate
		int k = 0;
		
		for(k = 0; k<Nmax; k++){
			
			getDerivative(P.getArray(), X, Y, Zn, J, R); // get Jacobbian matrix 
			
			double[][] Jm = new double[Lmask][Lp];
			double[][] Rm = new double[Lmask][1];
			int j = 0;
			for(int i=0; i<L; i++){
				if(Mask[i]){
					Jm[j][0] = J[i][0];
					Jm[j][1] = J[i][1];
					Jm[j][2] = J[i][2];
					Jm[j][3] = J[i][3];
					Jm[j][4] = J[i][4];
					
					Rm[j][0] = R[i][0];
					j++;
				}

				
			}
			
		    Matrix R1 = new Matrix(Rm);
	        Matrix J1 = new Matrix(Jm);
	        Matrix J2 = J1.transpose(); // get J'
	        //double[][] JJ1 = J1.getArrayCopy();
	        Matrix G = J2.times(R1);
	        Matrix d = ((J2.times(J1)).inverse()).times(G);  //get d = inv(J'J)*J'*R 	       
	       
	        double a = lineSearch(P, d, G);
	        d = d.times(a);
	        P.minusEquals(d); // P(k+1) = P(k) - a*d;
	        
	        if(k>5 && d.norm2()<1e-8){
	        	break;
	        }
	        
	        double[][] p = P.getArrayCopy();
	        //System.out.println(p[0][0]+" " + p[1][0]+" " + p[2][0]+" " + p[3][0]+" " + p[4][0]);
		}	
		//double[][] dd = P.getArrayCopy();
		p1 = P.transpose().getArray()[0];
		
		if(Double.isNaN(p1[1]) || Double.isNaN(p1[2]) || p1[1]<=Xl || p1[1]>=Xr || p1[2]<=Yl|| p1[2]>=Yr){
			return p0;
		}
		return p1;
		
	}
	
	
	double lineSearch(Matrix P, Matrix d, Matrix G){
		double a = 1,step = 1;
		double b = 0.1;
		double tao = 0.5;
		double e0,e1;
		e0 = objfun(P, Zn);
		Matrix T = G.transpose().times(d).times(-b); // p_k+1 =P_K - a*d;
		double e = T.get(0,0);
		for(int i=0; i<50; i++){
			e1 = objfun(P.minus(d.times(a)), Zn);
			
			if(e1<=e0+e*a){
				step = a;
				break;
			}
			a = tao*a;
		}
		return a;
		
	}
	
double[] fitLM(double[] p0, int Nmax){
		
		int Lp = 5;
		double[][] T = new double[Lp][1];
		double[][] E = new double[Lp][Lp];
		
		for(int i=0; i<Lp; i++){
			T[i][0] = p0[i]; 
	    	E[i][i] = 1; 
	    }
		
	    Matrix Eye = new Matrix(E);
		Matrix  P = new Matrix(T);
		Matrix  Pnew = P.copy();
		//RealMatrix P = new Array2DRowRealMatrix(T);
		
		double[][] R = new double[L][1];
		double[][] J = new double[L][Lp];		
		double[] p1 = p0.clone();
		// iterate
		int k = 0;
		double u=1000; 
		boolean flag = true;
	    Matrix R1 = null;
        Matrix J1 = null;
        Matrix J2 = null; // get J'
        double e1=0, e0=0;
		
		for(k = 0; k<Nmax; k++){
			
			if(flag){
				getDerivative(P.getArray(), X, Y, Zn, J, R); // get Jacobbian matrix 
				
				double[][] Jm = new double[Lmask][Lp];
				double[][] Rm = new double[Lmask][1];
				int j = 0;
				for(int i=0; i<L; i++){
					if(Mask[i]){
						Jm[j][0] = J[i][0];
						Jm[j][1] = J[i][1];
						Jm[j][2] = J[i][2];
						Jm[j][3] = J[i][3];
						Jm[j][4] = J[i][4];
						
						Rm[j][0] = R[i][0];
						j++;
					}
	
					
				}
				
				
			    R1 = new Matrix(Rm);
		        J1 = new Matrix(Jm);
		        J2 = J1.transpose(); // get J'
		        //double[][] JJ1 = J1.getArrayCopy();
		        
		        if(k==0){
		        	e0 = objfun(R);
		        }
			}
	        
	        
	        Matrix d = ((J2.times(J1)).plus(Eye.times(u))).inverse().times(J2).times(R1);  //get d = inv(J'J)*J'*R 
	        
	        Pnew = P.minus(d);  // P(k+1) = P(k) - d;
	        
	        e1 = objfun(Pnew, Zn);
	        
	        if(e1>e0){
	        	u = u*2;
	        	flag = false;
	        }else{
	        	P=Pnew.copy();
	        	u=u/2;
	        	e0 = e1;
	        	flag=true;
	        }
	        
	        
	        if(flag && (d.norm2()<1e-8)){
	        	break;
	        }
	        
	        //double[][] p = P.getArrayCopy();
	        //System.out.println(p[0][0]+" " + p[1][0]+" " + p[2][0]+" " + p[3][0]+" " + p[4][0]);
		}	
		//double[][] dd = P.getArrayCopy();
		p1 = P.transpose().getArray()[0];
		
		if(Double.isNaN(p1[1]) || Double.isNaN(p1[2]) || p1[1]<=Xl || p1[1]>=Xr || p1[2]<=Yl|| p1[2]>=Yr){
			return p0;
		}
		return p1;
		
	}
	
	
	
	//public static RealMatrix getInverse(RealMatrix A) {
     //   RealMatrix result = new RRQRDecomposition(A).getSolver().getInverse();
     //   return result; 
    //}	
	  double[] fitLSQ(double[] p0){
		  
		    if(Lmask<8){
		    	return p0;
		    }
			
			int Lp = 4;
			
			
			double[][] p = new double[Lp][1];
			double[][] B = new double[Lmask][1];
			
			Matrix  P = new Matrix(p);	
			double[][] A = new double[Lmask][4];
			double[] p1 = p0.clone();
			
			double Bg = p0[4];
			int k=0;
			for(int i=0; i<L; i++){
				
				if(Mask[i]){
					A[k][0] = X[i];
					A[k][1] = Y[i];
					A[k][2] = Math.log(Zn[i]+1);
					A[k][3] = 1;
					B[k][0] = X[i]*X[i]+Y[i]*Y[i];
					//bb[i] = X[i]*X[i]+Y[i]*Y[i];
					k++;
				}

			}
			
			Matrix B1 = new Matrix(B);
			Matrix A1 = new Matrix(A);
			Matrix A2 = A1.inverse();
			P = A2.times(B1);
			double[][] P1 = P.getArrayCopy();
			p1[1] = P1[0][0]/2;
			p1[2] = P1[1][0]/2;
			//p1[3] = -P1[2][0]/2;
			//p1[0] = Math.exp((a*a+b*b+P1[3][0])/(2*sig));
			//double[][] AA1 = A1.getArrayCopy();
			//double[][] AA2 = A2.getArrayCopy();
			// iterate
	        //double [][] values = {{1, 1, 2}, {2, 4, -3}, {3, 6, -5}};
	        //double [] rhs = { 9, 1, 0 };
			/*
	        RealMatrix A1 = new Array2DRowRealMatrix(A);
	        RealMatrix b1 = new Array2DRowRealMatrix(b);
	        RealMatrix P = new Array2DRowRealMatrix(p);
	        //RealMatrix A2 = getInverse(A1);
	        
	        RealVector b2 = new ArrayRealVector(bb);
	        QRDecomposition QRD = new RRQRDecomposition(A1);
	        //RealMatrix A2 = QRD.getSolver().getInverse();
	        RealVector A3 = QRD.getSolver().solve(b2);
	        */
	        //P = A2.multiply(b1);
			//39613.70578964278 62.199492846091275 50.918157310055626 3.4939369482222804 1766.5744320444041
			if(Double.isNaN(p1[1]) || Double.isNaN(p1[2]) || p1[1]<=Xl || p1[1]>=Xr || p1[2]<=Yl|| p1[2]>=Yr){
				return p0;
			}
			return p1;
			
		}
	
	  
	public static void main(String[] args) throws IOException{
		
		boolean[] Mask = new boolean[10];
		Arrays.fill(Mask, true);
		
		ImagePlus Image = new ImagePlus("D:\\GWDG\\VS2013\\OpenCVtest\\OpenCVtest\\ISM_Ref_crop.tif");
		ImageProcessor Ip = Image.getProcessor();
		Ip = Ip.convertToFloatProcessor();
		float[] pixel = (float[]) Ip.getPixels();
		int Width = Ip.getWidth();
		int Height = Ip.getHeight();
		
		//double[] subImage = new double[Height*Width];
		
		
		int H = 19;
		int W = 19;
		int Nr = H/2;
		int m = (Height+1)/2, n = (Width+1)/2;
		int k = 0;
		double[] subImage = new double[H*W];
		for(int i=m-Nr; i<=m+Nr; i++)
		for(int j=n-Nr; j<=n+Nr; j++){
			subImage[k] = pixel[i*Width+j]; // & 0xFFFF
			k++;
		}
		
		int Bg = 2000;
		double[] p0 = {90,0, 0, 9, 100}, p;
		Gaussianfitter fitter = new Gaussianfitter(subImage, W, H, 0-Nr, 0-Nr, null);
		p = fitter.fitGN(p0, 100);
		p = fitter.fitLM(p0,100);
		p = fitter.fitLSQ(p0);

		
		
		
		
	}
	
	
}

