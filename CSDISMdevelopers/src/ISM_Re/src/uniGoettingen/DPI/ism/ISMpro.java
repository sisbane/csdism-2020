package uniGoettingen.DPI.ism;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import Jama.Matrix;
import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.io.Opener;
import ij.process.FloatProcessor;
import ij.process.ImageProcessor;

public class ISMpro {
	public native static double[] LocalizePSF(short[] Iraw, int Width, int Height, short[] Iup, int Scale, int Np);
	public native static double[] copyPSF(double[] Image, double[] PX, double[] PY, int Len, int Np);
	public native static double[] FFT2D(double[] data,  int Width, int Height);
	public native static double[] IFFT2D(double[] data,  int Width, int Height);

	final static boolean FFT2D_FORWARD = true;
	final static boolean FFT2D_BACKWARD = false; 
	
	static int nProcessed = 0;	
	static String strData = "";
	
   static {
	   	  System.loadLibrary("libfftw3-3");
	      System.loadLibrary("ISMpro");
	      
   }
   /*
   static {
	   
	   String[] filename = {"libfftw3-3.dll", "ISMpro.dll"};
	   for(int j=0; j<2; j++){
		   
			try {
	
				File ffile = new File("");
				String filePath = null;
				filePath = ffile.getAbsolutePath() + File.separator	+ filename[j];				
				//System.out.println("filePath of dll is " + filePath);
				
				File dllFile = new File(filePath); 
				//System.out.println(dllFile.length());
				if(!dllFile.exists() || dllFile.length()<=0){
				   
					FileOutputStream out = new FileOutputStream(dllFile);
					
					InputStream in = ISMpro.class.getResourceAsStream("/"+filename[j]);
					System.out.println("ISMpro.class.getClass()" + in);
		
					int i;
					byte[] buf = new byte[1024];
					try {
						while ((i = in.read(buf)) != -1) {
							out.write(buf, 0, i);
						}
					} finally {
						in.close();
						out.close();
					}
				}
				System.out.println(dllFile.getAbsolutePath());
				System.load(dllFile.getAbsolutePath());//
				dllFile.deleteOnExit();

			} catch (Exception e) {
				System.err.println("load jni error!");
							}
		}
	}
	*/
	public static void main(String[] args) throws IOException{
	
		/*
		
		String dataStr = "";
		int Nframe = 1;
		ImagePlus Image = new ImagePlus("E:\\GWDG\\VS2013\\OpenCVtest\\OpenCVtest\\ISM_Ref_crop.tif");
		ImageProcessor Ip = Image.getProcessor();
		
		short[] data = (short[])Ip.getPixels();
		int Width = Ip.getWidth();
		int Height = Ip.getHeight();
		
		Mat I = new Mat(Height, Width, CvType.CV_16U);
		double[] d = I.get(1, 2);
		Core.dft(I,I);
		

		int Scale = 9;
		int Np = 9;
		Ip.setInterpolationMethod(ImageProcessor.BICUBIC);
		Ip = Ip.resize(Scale*Width, Scale*Height);
		short[] dataUp = (short[])Ip.getPixels();
		
		double[] PXY = LocalizePSF(data, Width, Height, dataUp, Scale, Np);
		int Len = PXY.length;
		System.out.println(PXY);
		
		
				Core.dft(complexImage, complexImage);
		
		Core.split(complexImage, newPlanes);
		Core.magnitude(newPlanes.get(0), newPlanes.get(1), mag);
		Core.dft(complexImage, complexImage);
		Core.split(complexImage, newPlanes);
		Core.magnitude(newPlanes.get(0), newPlanes.get(1), mag);
		
		
		Core.idft(complexImage, complexImage);
		Mat restoredImage = new Mat();
		Core.split(this.complexImage, this.planes);
		Core.normalize(this.planes.get(0), restoredImage, 0, 255, Core.NORM_MINMAX);
		*/
		
		
		 double[][] vals = {{1.,2.,3},{4.,5.,6.},{7.,8.,10.}};
	      Matrix A = new Matrix(vals);
	      Matrix Ai = A.inverse();
	      double[][] B = Ai.getArrayCopy();
		 System.out.print(B);
		
		String imgpath = "E:/GWDG/VS2013/OpenCVtest/OpenCVtest/ref405_Chroma_30c_250im_4p_25d_1_MMStack_Pos0.ome.tif";
		Opener opener = new Opener();
		final ImageStack imstack = opener.openImage(imgpath).getStack();		


		
		strData = "";
		final int Width = imstack.getWidth(); //interpolate before correlation calculation
		final int Height = imstack.getHeight();
		//Hsub = nPSFpixel*scale-1;
		//Wsub = nPSFpixel*scale-1;
		final int Nthread = 16;
		
		final int Scale = 9;
		final int Np = 9;
		
		
		final int NFrame = imstack.getSize();
		
		if(NFrame<=0){
			return;
		}
		
		//ImageProcessor Ip = imstack.getProcessor(1);
		
		ImagePlus IPlus = new ImagePlus("E:\\GWDG\\VS2013\\OpenCVtest\\OpenCVtest\\ISM_Ref_crop.tif");
		
		ImageProcessor Ip = IPlus.getProcessor();
		int W = Ip.getWidth();
		int H = Ip.getHeight();
		
		
		short[]pixels = (short[])Ip.getPixels();	
		int Len = pixels.length;
		double[] data = new double[Len];
		for(int j = 0; j<Len; j++){
			data[j] = (pixels[j]&0xFFFF);
		}

		double [] F = FFT2D(data, W, H);
		
		double [] I = IFFT2D(F, W, H);
		
		double [] II = new double[W*H];
		System.arraycopy(I, 0, II, 0, W*H);
		for(int j = 0; j<Len; j++){
			data[j] = data[j] - II[j];
		}
		
		
		double[] Amp = new double[Len];
		for(int j = 0; j<Len; j++){
			Amp[j] = Math.log10(Math.sqrt(F[j]*F[j]+F[j+Len]*F[j+Len]));
		}
		Ip = new FloatProcessor(W, H, data);
		ImagePlus ImPlus=  new ImagePlus("", Ip);
		ImPlus.show();
		//System.out.println("Welcome to OpenCV " + Core.VERSION);
	    //System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	    //Mat m  = Mat.eye(3, 3, CvType.CV_8UC1);
	    //System.out.println("m = " + m.dump());
		
		//Wism = 2*W;
		//Hism = 2*H;
		//ISMimage =  new float[Wism*Hism];
			
		for(int k=1; k<=Nthread; k++){
			
			final int kThread = k-1;           
			Thread thread = new Thread(){
				
				public void run(){
					for(int i=1; i<=NFrame; i++){///
						int nframe = i;
						if(i%Nthread == kThread){
							
							String strPXY = "";	
							ImageProcessor Ip = imstack.getProcessor(nframe);
							//if(!isBackgroundset){
								//background = (int) ip.getStatistics().dmode;
							//}
							
							short[] data = (short[])Ip.getPixels();		
							
							Ip.setInterpolationMethod(ImageProcessor.BICUBIC);
							ImageProcessor Ip1 = Ip.resize(Scale*Width, Scale*Height);
							short[] dataUp = (short[])Ip1.getPixels();
							
							double[] PXY = LocalizePSF(data, Width, Height, dataUp, Scale, Np);
							int Len = PXY.length;
							
							
							for(int j=0; j<Len/2; j++){
								strPXY = strPXY + PXY[2*j]+" "+ PXY[2*j+1] + " " + nframe + "\n";
							}
							
							strData = strData + strPXY;
							
							nProcessed++;
						}
						
					}
						
					}
					
					
			};
			thread.start();	
					
		}
		
		
		//wait for finishing of ISM image computing
		boolean isfinished = false;
		while(!isfinished){
			IJ.wait(1000);
			System.out.println("nProcessed = "+nProcessed+"|"+NFrame);
				if(nProcessed==NFrame){				

					System.out.println("nProcessed = "+nProcessed+"|"+NFrame);
					
					
					
			        String path= "E:\\GWDG\\VS2013\\OpenCVtest\\OpenCVtest\\"+"POS_XY.txt";
			        File file = new File(path);
	
			        // If file doesn't exists, then create it
			        if (!file.exists()) {
			            file.createNewFile();
			        }
	
			        FileWriter fw = new FileWriter(file.getAbsoluteFile());
			        BufferedWriter bw = new BufferedWriter(fw);
	
			        // Write in file
			        bw.write(strData);
	
			        // Close connection
			        bw.close();		
				
					}
			    }
				isfinished = true;
							
				//return ISMimage;
				//return normaliz(ISMimage);
		}
		
		
		
		

	
	public void ISMstackProcMul(final ImageStack imgstack, int Npixel, int Ns) throws IOException{
		


		strData = "";
		final int Width = imgstack.getWidth(); //interpolate before correlation calculation
		final int Height = imgstack.getHeight();
		//Hsub = nPSFpixel*scale-1;
		//Wsub = nPSFpixel*scale-1;
		
		final int Scale = Ns;
		final int Np = Npixel;
		
		int NFrame = imgstack.getSize();
		
		if(NFrame<=0){
			return;
		}
		//System.out.println("Welcome to OpenCV " + Core.VERSION);
	    //System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	    //Mat m  = Mat.eye(3, 3, CvType.CV_8UC1);
	    //System.out.println("m = " + m.dump());
		
		//Wism = 2*W;
		//Hism = 2*H;
		//ISMimage =  new float[Wism*Hism];
			
		for(int i=1; i<=NFrame; i++){///
			
			final int nframe = i;           
			Thread thread = new Thread(){
				public void run(){
					String strPXY = "";	
					ImageProcessor Ip = imgstack.getProcessor(nframe);
					//if(!isBackgroundset){
						//background = (int) ip.getStatistics().dmode;
					//}
					
					short[] data = (short[])Ip.getPixels();		
					
					Ip.setInterpolationMethod(ImageProcessor.BICUBIC);
					ImageProcessor Ip1 = Ip.resize(Scale*Width, Scale*Height);
					short[] dataUp = (short[])Ip1.getPixels();
					
					double[] PXY = LocalizePSF(data, Width, Height, dataUp, Scale, Np);
					int Len = PXY.length;
					
					
					for(int j=0; j<Len; j++){
						strPXY = strPXY + PXY[2*j]+" "+ PXY[2*j+1] + " " + nframe + "\n";
					}
					
					strData = strData + strPXY;
					
					nProcessed++;
					
		    }
			};
			thread.start();					
	
		}
		
		
		//wait for finishing of ISM image computing
		boolean isfinished = false;
		while(!isfinished){
			IJ.wait(1000);
			System.out.println("nProcessed = "+nProcessed+"|"+NFrame);
				if(nProcessed==NFrame){				

					System.out.println("nProcessed = "+nProcessed+"|"+NFrame);
					
					
					
			        String path= "E:\\GWDG\\VS2013\\OpenCVtest\\OpenCVtest\\"+"POS_XY.txt";
			        File file = new File(path);
	
			        // If file doesn't exists, then create it
			        if (!file.exists()) {
			            file.createNewFile();
			        }
	
			        FileWriter fw = new FileWriter(file.getAbsoluteFile());
			        BufferedWriter bw = new BufferedWriter(fw);
	
			        // Write in file
			        bw.write(strData);
	
			        // Close connection
			        bw.close();		
				
					}
			    }
				isfinished = true;
							
				//return ISMimage;
				//return normaliz(ISMimage);
		}
			
		

	
	

}