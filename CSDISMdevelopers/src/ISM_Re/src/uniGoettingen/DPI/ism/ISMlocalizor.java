package uniGoettingen.DPI.ism;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferInt;
import java.awt.image.WritableRaster;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.io.Opener;
import ij.process.FloatProcessor;
import ij.process.ImageProcessor;
import ij.process.ShortProcessor;

public class ISMlocalizor {
	

public static float[] getPSF(int N, double c){
		
		int i,j;
		float[] cx = new float[N*N];
		float[] cy = new float[N*N];
		float[] gauss = new float[N*N];
		float mid = (float) ((N-1)/2.0);
		for(i=0; i<N; i++)
			for(j=0; j<N; j++){				
	
				cx[i*N+j] = (j-mid);
				cy[i*N+j] = (i-mid);
			
		}
		
		double sigma;
		
		sigma = mid/Math.sqrt(Math.log(1/(c*c)));
		
		for(i=0; i<cx.length; i++){
			gauss[i] = (float) Math.exp(-(cx[i]*cx[i]+cy[i]*cy[i])/(2*sigma*sigma));		
			
		}
		//only use pixel within circle
		/*
		int indx = ((int)mid)*N;
		for(i=0; i<cx.length; i++){
			if(gauss[i]<gauss[indx]){
				gauss[i] = 0;
			}
			
		}*/
		
		
		return gauss;	
	}
	

public static double[] avgPSF(double[] Gauss){
		int i;
		double sum = 0;
		int Ng = 0;
		
		if(Gauss==null){
			return null;
		}
		int Len = Gauss.length;

		double[] PSF = new double[Len];
		for(i=0; i<Gauss.length; i++){
			PSF[i] = Gauss[i];
		}
		
		for(i=0; i<PSF.length; i++){
			if(PSF[i]>0){
				sum+=PSF[i];
				Ng++;
			}
			
		}
		double avg = sum/Ng;
		for(i=0; i<PSF.length; i++){
			if(PSF[i]>0){
				PSF[i]-=avg;
			}
			
		}
		return PSF;
	}

public static double normPSF(double[] Gauss){
	
	int i;
	double sum = 0;
	if(Gauss==null){
		return 0;
	}
	int Len = Gauss.length;
	for(i=0; i<Len; i++){
		sum += Gauss[i]*Gauss[i];
	}
	
	return Math.sqrt(sum);
}
	
public static double[] padarray(float[] img, int M, int N, int row, int col){
	
	int i,j;
	int M1 = M + 2*row;
	int N1 = N + 2*col;
	double[] newarray = new double[M1*N1];

	
	for(i=0; i<M; i++)
		for(j=0; j<N; j++){
			
			newarray[(i+row)*N1+(j+col)] = img[i*N+j];
			
		}
	
	return newarray;
	
	
}


public static double[] padarray(double[] img, int M, int N, int row, int col){
	
	int i,j;
	int M1 = M + 2*row;
	int N1 = N + 2*col;
	double[] newarray = new double[M1*N1];

	
	for(i=0; i<M; i++)
		for(j=0; j<N; j++){
			
			newarray[(i+row)*N1+(j+col)] = img[i*N+j];
			
		}
	
	return newarray;
	
	
}


public static double[] evenSize(float[] img, int M, int N){
	
	int i,j;
	
	int row = 0;
	int col = 0;
	if(M%2!=0){
		row++;
	}
	
	if(N%2!=0){
		col++;
	}
	
	int Width = N+col;
	double[] newarray = new double[M*N+row*N+col*M];
	
	for(i=0; i<M; i++)
		for(j=0; j<N; j++){
			
			newarray[i*Width+j] = img[i*N+j];
			
		}
	
	return newarray;
	
	
}	
	
public static double[] circShift(double[] img,  int N, int M,int Rows, int Cols){
	
	int i,j;
	double[] newImg = new double[M*N];
	
	for(i=0; i<M; i++)
	for(j=0; j<N; j++){		
		
		
		
		newImg[((i+Rows+M*(Rows<0?1:0))%M)*N + (j+Cols + N*(Cols<0?1:0))%N] = img[i*N+j];
	}
	
	return newImg;
}


public static double[] findpeaks(double[] R, int N, int M, double th){
	
	int len = M*N;
	
	int k = 0;
	double[] PXY = new double[1000000];
	
	double pixel, left, right, up, down, leftup, rightup, leftdown, rightdown;
	
	for(int i=0; i<M; i++)		
	    for(int j=0; j<N; j++){
		    pixel = R[i*N+j];
		    int idx;
		    
		    idx = (i-1)*N+j;
		    if(idx>0 && idx<len){
		    	up =  R[idx];
		    }else{
		    	up = 0;
		    }
			
		    idx = (i+1)*N+j;
		    if(idx <len){
		    	down = R[idx];
		    }else{
		    	down = 0;
		    }
			
		    idx = i*N+j-1;
		    if(idx>0 && idx<len){
		    	left = R[idx];
		    }else{
		    	left = 0;
		    }
		    
		    idx = i*N+j+1;
			if(idx<len){
				right = R[idx];
			}else{
				right = 0;
			}
			
			idx = (i-1)*N+j-1;
			if(idx >0 && idx<len){
				leftup = R[idx];					
			}else{
				leftup = 0;
			}
			
			idx = (i-1)*N+j+1;
			if(idx>0 && idx<len){
				rightup = R[idx];
			}else{
				rightup = 0;
			}
			
			idx = (i+1)*N+j-1;
			if(idx>0 && idx<len){
				leftdown = R[idx];
			}else{
				leftdown = 0;
			}
			
			idx = (i+1)*N+j+1;
			if(idx >0 && idx<len){
				rightdown = R[idx];
			}else{
				rightdown = 0;
			}
						
			if(pixel>th && pixel>=left && pixel>=right && pixel>=up && pixel>=down && pixel>=leftup && pixel>=rightup && pixel>=leftdown && pixel>=rightdown){
								
				PXY[2*k] = j;
				PXY[2*k+1] = i;
				k++;
			}
	}
	
	if(k<1){
		return null;
	}
	double[] Peak = new double[2*k];
	System.arraycopy(PXY, 0, Peak, 0, 2*k);
	//System.out.println("Total peak number="+ k);
	return Peak;
	
	
}

	





	 public static void main(String[] args){
			
		 //String filedir = "C:/Users/Administrator/Desktop/xy.txt";
		String filedir = "E:/GWDG/ISM/data/pos1.txt";//locationxy.txt";
		
		//load image stack
		//String filename = "E:/GWDG/ISM/data/IHCs_p14_calretinin488_Vglut568_Otof647/blue_1/blue_1_MMStack_Pos0.ome.tif"; //green_2/green_2_MMStack_Pos0.ome.tif"; //blue_1/blue_1_MMStack_Pos0.ome.tif";
		String imgpath = "E:/GWDG/VS2013/OpenCVtest/OpenCVtest/ref405_Chroma_30c_250im_4p_25d_1_MMStack_Pos0.ome.tif";
		Opener opener = new Opener();
		final ImageStack imstack = opener.openImage(imgpath).getStack();	
		//ImageProcessor ip = imstack.getProcessor(1);
		
		
		//imStackLocalizer(imgpath, 10, 9, 2000,null);
		
		
		
		
		ImagePlus IPlus = new ImagePlus("E:\\GWDG\\VS2013\\OpenCVtest\\OpenCVtest\\ISM_Ref_crop.tif");			
		ImageProcessor ip = IPlus.getProcessor();
		
		ip = ip.convertToFloatProcessor();
		
		
		int  Height= ip.getHeight();
		int Width = ip.getWidth();
		int len = Width*Height;
		double[] I11 = padarray((float[])ip.getPixels(), Height, Width, 0, 0);
		//double [] F11 = ISMpro.FFT2D(I11, Width, Height);	
		
		//double [] F12 = ISMpro.IFFT2D(F11, Width, Height);	
		
		//System.out.flush();

			
		int Scale = 1;
		int Np = 9;
		int Mnew = Height;
		int Nnew = Width;
		
		int row = 0;
		int col = 0;
		//make sure that the number of row and col are even
		if(Width%2!=0){
			col = 1;
			Nnew = Nnew + 1;				
		}
						
		if(Height%2!=0){
			row = 1;
			Mnew = Mnew + 1;
		}
				
		
		float[] Pixels = (float[]) ip.getPixels();
		
		
		double[] I1 = new double[Height*Width+row*Width+col*Height];
		
		int j;
		for(int i = 0; i<Height; i++)
			for(j=0; j<Width; j++){
				
				I1[i*Nnew+j] = Pixels[i*Width+j];
				
			}
		
		
		if(Np%2!=0){
			Np++;
		}
		
		int Mpad = (Mnew-Np)/2;
		int Npad = (Nnew-Np)/2;
		int Limage = Mnew*Nnew;
		
		double normPSF = 0;
		float PSF[] = getPSF(Np,0.1);
		for(int i=0; i<PSF.length; i++){
			normPSF += PSF[i]*PSF[i];
		}
				
		normPSF = Math.sqrt(normPSF);
		
		
		//FloatProcessor ip1 = new FloatProcessor(Nnew, Mnew, padarray(PSF, Np, Np, Mpad, Npad));			 
		//FHT OTF = new FHT(ip1);
		//OTF = OTF.getCopy();
		//OTF.transform();					
		
		double[] PSF1 = padarray(PSF, Np, Np, Mpad, Npad);

		
		
		//get window function	
		float[] W = new float[Np*Np]; 
		for(int i=0; i<W.length; i++){
			W[i] = 1;
		}
		double[] W1 = padarray(W, Np, Np, Mpad, Npad);			 
		
		//FloatProcessor ip3 = new FloatProcessor(Nnew, Mnew, P2);

		// calculate the square of image
		


		//double [] OTF = ISMpro.FFT2D(PSF1, Nnew, Mnew);
		//double [] FW = ISMpro.FFT2D(W1, Nnew, Mnew);
		
		
		
		//FHT C1 = FI1.multiply(OTF);
		//C1.inverseTransform();
		
		//FHT C2 = FI2.multiply(FW);
		//C2.inverseTransform();
		//C2.sqrt();

		
		//FloatProcessor R = new FloatProcessor(Nnew, Mnew, C);
		//R.swapQuadrants();
		//ImageProcessor ps = FI1.getPowerSpectrum();
	    //ImagePlus ImPlus=  new ImagePlus("", R);
	    //ImPlus.show();
		
		
		
		
		
		
	 }
	 
	 
		public static double[] findLightSpot(double[] I, double[] Corr, int Width, int Height, double[] PSF, int Np, double normPSF, boolean Estimate){			
			

			//short[] PSF2 = doub2short(PSF1, M3, N3);
			//ism.imshow(PSF2, M3, N3);
			
			int Nr = (Np-1)/2;
			int Lp = Np*Np;
			int m,n;
			int l;
			int i,j, k;
			
			//double[] C = new double[I.length];
			double[] C = Corr;
			
			double Cmax = 0.1;
			double[] PSF1 = avgPSF(PSF);
			double normPSF1 = normPSF(PSF1);
			//calculate correlation
			for(i=0; i<Height; i++)
				for(j=0; j<Width; j++){
					
					k = 0;
					int pixels = 0;
					double[] sub = new double[Lp];
					//copy sub-image
					for(m=i-Nr; m<=i+Nr; m++)
						for(n=j-Nr; n<=j+Nr; n++){
							if(m>=0 && m<Height && n>=0 && n<Width && PSF[k]>0){
								sub[k] = I[m*Width+n];	
								pixels++;
							}								
							k++;
						}
					
					double sum = 0;
					for(k=0; k<Lp; k++){
						sum += sub[k];
					}
					double avg = sum/pixels;
					
					for(k=0; k<Lp; k++){
						if(PSF[k]>0 ){
							sub[k]-= avg;
						}
						
					}
														
					double Cor = correlation(sub, PSF1, normPSF1);
					
					C[i*Width+j] = Cor;
					if(Cor>Cmax){
						Cmax = Cor;
					}
					
			}
			//FloatProcessor R = new FloatProcessor(Width, Height, C);
		    //ImagePlus ImPlus=  new ImagePlus("", R);
		    //ImPlus.show();
			double[] PXY;
			if(Estimate){
				PXY = findpeaks(C, Width, Height, Cmax*0.70);
			}else{
				PXY = findpeaks(C, Width, Height, Cmax*0.60);
			}
			
			/*
			if(Estimate){
				
				int Nspot = 0;
				
				if(PXY==null){
					PXY = new double[2];
					PXY[0] = 0;
					PXY[1] = 0;
					return PXY;
				}
				Nspot = PXY.length/2;
				//int m, n;
				double max = 0;;
				
				int idx = Nspot/2;
				double[] Arr = new double[Nspot];
				double Sum = 0;
				for(k=0; k<Nspot; k++){
					
					n = (int) PXY[2*k];
					m = (int) PXY[2*k+1];
					Arr[k] = C[m*Width+n]; 
					Sum += C[m*Width+n];
					
					if(C[m*Width+n]>max){
						max = C[m*Width+n];
						idx = k;
					}
				}
				PXY[0] = Sum;
				PXY[1] = Nspot;			
			} */
			
			return PXY;
		}

		static void imStackLocalizer1(String RefPath, int upScale, int Np, int Background, ISMImageProcessorGUI ismipGUI){
			
			
			ismipGUI.lbl_ProgMessage.setText("");
			//String imgpath = "E:/GWDG/VS2013/OpenCVtest/OpenCVtest/ref405_Chroma_30c_250im_4p_25d_1_MMStack_Pos0.ome.tif";
			if(!(RefPath.endsWith(".tif") || RefPath.endsWith(".tiff"))){
				
				JOptionPane.showMessageDialog(ismipGUI, "To do Localize, a tif/tiff file is required! ");
				return;
			}
			
			Opener opener = new Opener();
			
			ismipGUI.lbl_ProgMessage.setText("Loading image, please wait...");

			ImageStack imstack = opener.openImage(RefPath).getStack();	
			
			//ImagePlus IPlus = new ImagePlus("E:\\GWDG\\VS2013\\OpenCVtest\\OpenCVtest\\ISM_Ref_crop.tif");			
			//ImageProcessor ip = IPlus.getProcessor();
			
			int Height= imstack.getHeight();
			int Width = imstack.getWidth();
			
			ImagePlus Imp = IJ.createImage("My new image", "32-bit black", Width, Height, 1);

			if(Np%2==0){
				Np++;		//Np should be odd
			}
			
			
			//Height = Height*Scale;
			//Width = Width*Scale;
			//Np = Np*Scale;
			double pix;
			double Bg = Background;
			double sigma = ismipGUI.ism.FWHM/ismipGUI.ism.PixelSize/2.355;
			//double sigma = 1.6;		
			//System.out.flush();
				
			int Mnew = Height;
			int Nnew = Width;
			
			int Nr = Np/2;
			Mnew = Mnew + Nr*2;
			Nnew = Nnew + Nr*2;
			
			
			int Limage = Mnew*Nnew;
			
			double normPSF = 0;
			float[] PSF = getPSF(Np,0.1);
			double[] PSF1 = new double[Np*Np];
			for(int i=0; i<PSF.length; i++){
				normPSF += PSF[i]*PSF[i];
				PSF1[i] = PSF[i];
			}
					
			normPSF = Math.sqrt(normPSF);
			
			String Datastr = "GF_Localization -- format: X(nm)  Y(nm) FrameId "+ "\n";
			
				
			int Nframe = imstack.getSize();
			ismipGUI.progressBar.setMaximum(Nframe);
			
			boolean[] Mask = new boolean[Np*Np];
			
			
			for (int m = 1; m<=Nframe; m++){
				
				if(ismipGUI.isWindowClosed || ismipGUI.isStop){
					return; // stop thread when Jframe is colsed
				}				
				
				if(m==15){
					int mm =0;
					int nn = mm;
				}
				ImageProcessor ip = imstack.getProcessor(m);
				ip = ip.convertToFloatProcessor();
				//Bg = (int) ip.getStatistics().dmode;
				float[] Pixels = (float[]) ip.getPixels();
				//Bg = ip.getBackgroundValue();
				double[] I1 = new double[Limage];
				
				// estimate the value of sigma and background firstly
				if(m==1){
					//Bg = ip.getHistogramMax();
					ismipGUI.lbl_ProgMessage.setText("Estimating Sigma...");
					double[] Est = estimatePSF2(Pixels, Width, Height, Np,sigma,Bg);
					sigma = Est[0];
					System.out.println("Estimated Sigma: ="+sigma);
					if((Est[1]>=0) && (Est[1]<ip.getMax()*0.98)){//Est[1]>Bg/2 && Est[1]<Bg*1.5
						Bg = Est[1];
					}
					
					Np = (int) Math.round(sigma*2.355*2);
					if(Np%2==0){
						Np++;
					}
					
					normPSF = 0;
					PSF = getPSF(Np,Math.exp(-(Np-1)*(Np-1)/4.0/(2*sigma*sigma)));
					PSF1 = new double[Np*Np];
					for(int i=0; i<PSF.length; i++){
						normPSF += PSF[i]*PSF[i];
						PSF1[i] = PSF[i];
					}
							
					normPSF = Math.sqrt(normPSF);
					
					ismipGUI.lbl_ProgMessage.setText("Start localizing...");
				}

				//load image data of each frame
				for(int i = 0; i<Height; i++)
					for(int j=0; j<Width; j++){
						pix = Pixels[i*Width+j];
						I1[(i+Nr)*Nnew+(j+Nr)] = pix;
								
					}
					
				// estimate a optimal PSF
				if(m==0){

					ismipGUI.lbl_ProgMessage.setText("Estimating Sigma...");
					
					double c = estimatePSF1(I1, Nnew, Mnew, Np);					
					System.out.println("Estimated PSF: c ="+c);
					
					//update PSF
					normPSF = 0;
					PSF = getPSF(Np,c);
					PSF1 = new double[Np*Np];
					for(int i=0; i<PSF.length; i++){
						normPSF += PSF[i]*PSF[i];
						PSF1[i] = PSF[i];
					}
							
					normPSF = Math.sqrt(normPSF);
					float mid = (float) ((Np-1)/2.0);
					sigma = mid/Math.sqrt(Math.log(1/(c*c)));
					
					ismipGUI.lbl_ProgMessage.setText("Start localizing ...");
				}		
				//localize all measured light spot
				int Nspot = 0;				
				
				double[] I3 = new double[Height*Width];
				for(int i = 0; i<Height; i++)
					for(int j=0; j<Width; j++){
						
						pix = Pixels[i*Width+j];
						I3[i*Width+j] = pix;
								
					}
				double[] Corr = new double[Height*Width];
				double[] I4 = I3.clone();  //remove background
				for(int i=0;i<I4.length;i++){
					I4[i] = I4[i]>Bg?(I3[i]-Bg):0;
				}
				double[] PXY = findLightSpot(I4, Corr, Width, Height, PSF1, Np, normPSF, false);
				//double[] PXY = findLightSpot(I1, Nnew, Mnew, PSF1, Np, normPSF, false);
				if(Corr!=null){
					
					float[] C1 = new float[Corr.length];
					for(int p=0; p<C1.length; p++){
						C1[p] = (float) Corr[p];
					}	
	
					//ImageProcessor imProcessor = Imp.getProcessor();
					//imProcessor.setPixels(C1);
					//IJ.saveAsTiff(Imp,"D:\\GWDG\\ISM\\data\\DMD_ISM\\Untitled_1\\Corr.tif");
					
					//IJ.run(Imp, "Enhance Contrast", "saturated=0.5 normalize");
					//Imp.show();
				}
				/**/
				
					
				if(PXY!=null){					
					
					int Scale = 1;
					int Npad = Nr;
					if(Scale>1){
						
						int Hup = Scale*Height;
						int Wup = Scale*Width;
						//ip.setInterpolate(true); 
						ip.setInterpolationMethod(ImageProcessor.BICUBIC); 
						//Bg = ip.getBackgroundValue();
						ip = ip.resize(Hup, Wup);
						Pixels = (float[]) ip.getPixels();
						
						
						Npad = Nr*Scale;
						int Wnew = Wup +2*Npad;
						int Hnew = Hup +2*Npad;
						//float pix;
						double[] I2 = new double[Wnew*Hnew];
						for(int i = 0; i<Hup; i++)
							for(int j=0; j<Wup; j++){
								pix = Pixels[i*Wup+j];
								if(pix>Bg){
									I2[(i+Npad)*Wnew+(j+Npad)] = pix-Bg;	
								}
											
							}
						
						for(int i=0; i<PXY.length; i++){
							PXY[i] = (PXY[i]-Nr)*Scale+Npad;
						}
						
						fitLightSpot(I3, Corr, Wnew, Hnew, PXY, Np*Scale, Scale*sigma, Bg, 0);
						
					}
					else{
						//fitting localizing	
						//fitLightSpot(I1, Nnew, Mnew, PXY, Np, sigma, 0);
						//double[] PXY3 = PXY.clone();
						//fitLightSpot(I3, Width, Height, PXY, Np, sigma, 0);
						int Npixel = (int) Math.round(sigma*2.355*2);
						fitLightSpot(I3, Corr, Width, Height, PXY, Npixel, sigma, Bg, 0);
					}
					
					
					// display the position of detected light spots
					if(PXY!=null){
						int px = 0, py = 0;
						float[] LiSpots = new float[Width*Height]; 
						for(int i=0; i<PXY.length/2; i++){
							px = (int) Math.round(PXY[2*i]);
							py = (int) Math.round(PXY[2*i+1]);
							if(px<0 || px>=Width || py<0 || py>=Height){
								continue;
							}
							LiSpots[py*Width+px] = 1;
						}

						BufferedImage bufImage = ismipGUI.ism.transf2BufferedImage(LiSpots, Width, Height, 0, 0);
						ismipGUI.bufImage = bufImage;
						ismipGUI.repaint();
						
						
		
						
					}
					
					// save localization data into txt file
					/*
					for(int i=0; i<PXY.length; i++){
						if(PXY[i]>=Npad){
							PXY[i] = (PXY[i] - Npad)/Scale;	
						}else{
							PXY[i] = 0;
						}
						
					}*/
					
					String PXYstr = ""; 
					int k=0;
					if(PXY!=null){
						double PosX, PosY;
						for(k=0; k<PXY.length/2; k++){
							PosX = PXY[2*k];
							PosY = PXY[2*k+1];
							if(PosX>0 && PosY>0 && PosX<Width && PosY<Height){
								PXYstr = PXYstr + " " + String.format("%.5f", PosX*ismipGUI.ism.PixelSize) + " "+ String.format("%.5f", PosY*ismipGUI.ism.PixelSize) + " "+ m +"\n";
							}							
						}
					}
					
					Datastr = Datastr + PXYstr;
					Nspot = k;
				}
				System.out.println("Processed Frame number: " + m +"LightSpots:"+Nspot);
				
				ismipGUI.progressBar.setValue(m);
				ismipGUI.lblNf.setText(""+m);
				ismipGUI.lbl_ProgMessage.setText("Light_spots found: " + Nspot);
				
			}
			
			String RefDir  = RefPath;
			int ind = RefPath.lastIndexOf(".");
			if(ind>=1){
				RefDir = RefPath.substring(0, ind);
			}
			
			//save result to txt
			if(RefDir!=null && !RefDir.isEmpty()){
				try{
		            // Create new file           
		            String path= RefDir +".Localiz.txt";
		            File file = new File(path);

		            // If file doesn't exists, then create it
		            if (!file.exists()) {
		                file.createNewFile();
		            }else{
		            	file.delete();
		            	file.createNewFile();
		            }

		            FileWriter fw = new FileWriter(file.getAbsoluteFile());
		            BufferedWriter bw = new BufferedWriter(fw);

		            // Write in file
		            bw.write(Datastr);

		            // Close connection
		            bw.close();
		            
		            
					ismipGUI.lbl_ProgMessage.setText("Localization data has been saved into txt file!");
					IJ.wait(300);
					ismipGUI.lbl_ProgMessage.setText("Localization done!");
		            
				}catch(Exception e){
		            System.out.println(e);
		       }
		    }
			
			
		}
		
		
		static double estimatePSF1(double[] I, int Width, int Height, int Np){
			
				
			
			double start = 0.001, end = 0.25, step = 0.01;
			int Lc = (int) ((end - start)/step);
			double[] Rc = new double[Lc];
			int[] Ac = new int[Lc];
			int k = 0;
			int i, j;
			
			for(double c=start; c<end && k<Lc; c+= step){
				 
				double normPSF = 0;
				float PSF[] = getPSF(Np, c);
				double[] PSF1 = new double[Np*Np];
				for(j=0; j<PSF.length; j++){
					normPSF += PSF[j]*PSF[j];
					PSF1[j] = PSF[j];
				}			
				normPSF = Math.sqrt(normPSF);
				double[] Corr = new double[Height*Width];
				double[] PXY = findLightSpot(I, Corr, Width, Height, PSF1, Np, normPSF, true);
				Rc[k] = PXY[0]; // sum of correlation of all peaks
				Ac[k] = (int) PXY[1]; // number of peaks found
				k++;
			}
			
			int[] Bc = Ac.clone();
			Arrays.sort(Ac);
			int Lhis = Ac[Lc-1] - Ac[0] + 1;
			
			int[] His = new int[Lhis];
			for(i=0; i<Lc; i++){
				if(Ac[i]>0){
					His[Ac[i] - Ac[0]]++;
				}
			}
			
			int idx = His.length/2;
			int Hmax = His[idx];
			for(i=0; i<His.length; i++){
				if(His[i]>Hmax){
					Hmax = His[i];
					idx = i;
				}
			}

			double Rmax = 0; k = -1;
			int Nspot = idx+Ac[0];
			for(i=0; i<Rc.length; i++){
				if(Bc[i]==Nspot && Rc[i]>Rmax){
					Rmax = Rc[i];
					k = i;
				}
			}
			
			double c = 0.1;
			if(k>=0){
				c = start + k*step;
			}
			
			return c;
						
		}	
		
		
		static double[] estimatePSF2(float[] I, int Width, int Height, int Np, double sig0, double Bg){
			
			int k = 0;
			int i, j;
			double[] Background = null;
			double[] Sigma = null;
			double[] Est = new double[2];
			
			double normPSF = 0;
			float PSF[] = getPSF(Np, 0.05);
			double[] PSF1 = new double[Np*Np];
			for(j=0; j<PSF.length; j++){
				normPSF += PSF[j]*PSF[j];
				PSF1[j] = PSF[j];
			}			
			normPSF = Math.sqrt(normPSF);
			
			int Limg = I.length;
			double[] Pixels = new double[Limg];
			for(j=0; j<Limg; j++){
				Pixels[j] = I[j];
			}
			double[] Corr = new double[Height*Width];
			double[] PXY = findLightSpot(Pixels, Corr, Width, Height, PSF1, Np, normPSF, true);
			if(PXY!=null){
			
				int Nspots = PXY.length/2;
				
				Background = new double[Nspots];
				Sigma = new double[Nspots];
				
				
				if(Np%2==0){
					Np--;
				}
				
				int Nr = (Np-1)/2;
				int m, n;				
						
				double[] subImage = new double[Np*Np];
				int nest = 0;
				for(k=0; k<Nspots; k++){
					n = (int) PXY[2*k];
					m = (int) PXY[2*k+1];
					
					
					if(n<Np && m<Np && n>Width-Np && m>Height-Np){
						continue;
					}
					
					double x0 = n;
					double y0 = m;
					int p=0; 
					for(i=m-Nr; i<=m+Nr; i++)
						for(j=n-Nr; j<=n+Nr; j++){
							if(i>=0 && i<Height && j>=0 && j<Width){
								subImage[p] = Pixels[i*Width+j];
								/*if(subImage[p]>Am){
									Am = subImage[p];
									x0 = j;
									y0 = i;
								}*/
							}
							p++;
						}
							
					double[] q0 = estimateIni(subImage, Np, Np, -1);
					
					//q0[3] = sig0;
					//q0[4] = Bg;
					//double[] q0 = {Am, x0, y0, sigma, 0};
					double[] q = q0.clone();
					double sig = sig0;
										
					try{
						
						boolean[] Mask = new boolean[Np*Np];
						Arrays.fill(Mask, true);
						
						Gaussianfitter fitter = new Gaussianfitter(subImage, Np, Np, 0, 0, Mask);
								
						q[3] = sig0*sig0;
						q = fitter.fitLM(q, 100);
						if(q[3]>0){
							sig = Math.sqrt(q[3]);
						}
						
					} catch (Exception e) {
						
					}
					
					if(sig>0 && sig*2.35482/4 < Nr && q[0]>0 && q[4]>0){
						Sigma[nest] = sig;
						Background[nest] = q[4];
					}else{
						Sigma[nest] = -1;
						Background[nest] = -1;						
					}
					nest++;
					
				}
				
				int Ndet = 0;
				for(k=0; k<nest; k++){
					if(Sigma[k]>0){
						Est[0] += Sigma[k];
						Est[1] += Background[k];
						Ndet++;
					}

				}
				
				Est[0] /= Ndet;
				Est[1] /= Ndet;
				if(Est[0]<0.000001){
					Est[0] = sig0;
					Est[1] = Bg;
				}
			}
			
			
			return Est;
			
		}
		
		public static double correlation(double [] x1, double [] x2, double normX2){
			
			int len1 = x1.length;
			int len2 = x2.length;
			if(len1 != len2){
				return 0;
			}
			
			double r = 0;
			double sum = 0;
			double sum1 = 0;
			for(int i=0; i<len1; i++){
				sum = sum + x1[i]*x2[i];
				sum1 = sum1 + x1[i]*x1[i];
				//sum2 = sum2 + x2[i]*x2[i];
			}
			
			if(sum1>0){
				 r = sum/(Math.sqrt(sum1)*normX2);
			}
			
			if(Double.isNaN(r)){//r<0 || r>1 || 
				r = 0;
			}
			
			return r;
			
			
	}	
		
		static double[] estimateIni(double[] Image, int Width, int Height, double Bg){
			//FloatProcessor  Ip = new FloatProcessor(Image);
			int Ns = 4;
			double[] p0 = new double[5];
			
			int Limg = Image.length;
			float[] subImg = new float[Limg];
			for(int n=0; n<Image.length; n++){
				subImg[n] = (float) Image[n];
			}
			
			ImageProcessor Ip = new FloatProcessor(Width, Height);
			Ip.setPixels(subImg);
			Ip.setInterpolationMethod(ImageProcessor.BILINEAR);
			int Wid = Width*Ns;
			int Hei = Height*Ns;
			Ip = Ip.resize(Wid, Hei);
			if(Bg<0){
				p0[4] = Ip.getMin();
			}else{
				p0[4] = Bg;
			}
			p0[0] = Ip.getMax()-p0[4];
			
				
			p0[1] = Width/2.0;
			p0[2] = Height/2.0;

			/*
			int x0 = 0; 
			int y0 = 0;
			float[] pixel = (float[]) Ip.getPixels();
			
			double maxpixel = 0;
			for(int i=0; i<Hei; i++)
				for (int j=0; j<Wid; j++){
					
					if(pixel[i*Wid+j]>maxpixel){
						x0 = j;
						y0 = i;
						maxpixel = pixel[i*Wid+j];
					}
			}
	
			if(x0<=0 && y0<=0){				
				x0 = (int) (p0[1]*Ns); 
				y0 = (int) (p0[2]*Ns);
			}else{
				p0[1] = x0/Ns;
				p0[2] = y0/Ns;
			}
			
			float sigx = 0;
			for(int i=0; i<Hei; i++){				
				if(pixel[i*Wid+x0]>p0[0]/2){
					sigx++;
					}
			}
			
			
			double sigy = 0;
			for(int j=0; j<Wid; j++){				
				if(pixel[y0*Wid+j]>p0[0]/2){
					sigy++;
					}
			}
			double sig = (Wid+Hei)/2;
			if(sigx>0 && sigy>0){
				sig = (sigx + sigy)/2;
				sig = sig/(Ns*2.35482);
			}
			p0[3] = sig;
			
			*/
			
			return p0;
		}
		
		
		static void fitLightSpot(double[]Image, double[] Corr, int Width, int Height, double[] PXY, int Np, double sigma, double Bg, double th){
			
			if(PXY==null){
				return;
			}
			
			if(Np%2==0){
				Np--;
			}
			
			int Nr = (Np-1)/2;
			int m, n, i, j;
			
			//sigma = sigma*sigma;	
			
			double[] subImage = new double[Np*Np];
			double[] subCorr = new double[Np*Np];
			
			for(int k=0; k<PXY.length/2; k++){
				n = (int) PXY[2*k];
				m = (int) PXY[2*k+1];
				
				int p=0; 
				subImage = new double[Np*Np];
				subCorr = new double[Np*Np];
				for(i=m-Nr; i<=m+Nr; i++)
					for(j=n-Nr; j<=n+Nr; j++){
						if(i>=0 && i<Height && j>=0 && j<Width){
							subImage[p] = Image[i*Width+j];
							subCorr[p] = Corr[i*Width+j];
							/*if(subImage[p]>Am){
								Am = subImage[p];
								x0 = j;
								y0 = i;
							}*/
						}
						p++;
					}
						
				
				double[] q = fitPSF(subImage, Np, Nr, n, m, th,sigma, Bg);
				
				boolean Flag=false;
				double qx=q[1],qy=q[2];
				if(Math.abs(q[1]-n)>2.5){ 					
					//q[1]=n;	
					Flag = true;
					//q = fitPSF(subCorr, Np, Nr, n, m, th);	
					//System.out.println(n + " " + m +" " + q[1] + " " + q[2]);
				}
				if(Math.abs(q[2]-m)>2.5){
					//q[2]=m;
					Flag = true;
				}
				if(Flag){
					System.out.println(n + " " + m +" " + qx + " " + qy);
				}
				
				double px = q[1];
				double py = q[2];//*0-1
				
				if(px>=0 && px<Width && py>=0 && py<Height){
					
					PXY[2*k]  =  px;
					PXY[2*k+1] = py;
					
				}
				
				
			}


		}	
		
		static double[] fitPSF(double[] subImage, int Np, int Nr, int n, int m, double th, double sigma, double Bg){
			
			
			double[] q0 = estimateIni(subImage, Np, Np, Bg);
			
			
			//double[] q0 = {Am, x0, y0, sigma, 0};
			double[] q = q0.clone();
			
			boolean[] Mask = new boolean[Np*Np];
			Arrays.fill(Mask, true);
			
			if(th>0){
				for(int t=0; t<Mask.length; t++){
					if(subImage[t]>0){
						Mask[t] = true;
					}
					
				}
			}
			/**/
			
			Gaussianfitter fitter = new Gaussianfitter(subImage, Np, Np, n-Nr+0.5, m-Nr+0.5, Mask);
			/*try{
				q = fitter.fitLSQ(q);					
			} catch (Exception e) {
				q = q0.clone();
			} */
			
			try{
				//if(x0<103 && x0>97 && y0>67 && y0<69){
					//System.out.print("");
				//}
				q0[1] = n;
				q0[2] = m;
				q0[3] = sigma*sigma; //sigma
				q = fitter.fitLM(q0, 100);
			} catch (Exception e) {
				System.out.println("LM-Exception:" + q[0] + " " + q[1] + " " + q[2]+" " + q[3]+" " + q[4]);
				q = q0.clone();
				
			}
			
			boolean Flag=false;
			double qx=q[1],qy=q[2];
			if(Math.abs(q[1]-n)>2.5){ 					
				//q[1]=n;	
				Flag = true;
				//q = fitPSF(subCorr, Np, Nr, n, m, th);	
				//System.out.println(n + " " + m +" " + q[1] + " " + q[2]);
			}
			if(Math.abs(q[2]-m)>2.5){
				//q[2]=m;
				Flag = true;
			}
			if(Flag){		
				try{
					q = fitter.fitGN(q0, 80);
					System.out.println(n + " " + m +" " + qx + " " + qy);
				}catch (Exception e) {
						System.out.println("GN-Exception:" + q[0] + " " + q[1] + " " + q[2]+" " + q[3]+" " + q[4]);
						q = q0.clone();
						
					}
			}			
			
			
			return q;
		}
		
}
