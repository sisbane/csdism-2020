// ISM Image Processor: A open source software for Image Scanning microscopy image reconstruction
//
//     Copyright (C) 2019  Shun Qin, shun.qin@phys.uni-goettingen.de
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//     We expect you would cite our article, to which this source code project attach.



package uniGoettingen.DPI.ism;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import ij.ImageStack;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.awt.event.ActionEvent;
import javax.swing.JRadioButton;
import java.awt.event.InputMethodListener;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferShort;
import java.awt.event.InputMethodEvent;
import javax.swing.JProgressBar;
import javax.swing.UIManager;
import java.awt.SystemColor;
import java.awt.Toolkit;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JFormattedTextField;

public class ISMImageProcessorGUI extends JFrame {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 3317502136836903003L;
	private JTextField textField_Ref;
	private JTextField textField_Img;
	JFileChooser fChooser;
	Drawpanel drawpanel = new Drawpanel();
	private JPanel panel_ImageView;
	 BufferedImage bufImage = null;
	 ISMprocessor ism = null;
	 JProgressBar progressBar = null;
	 private JSpinner FWHM_textField;
	 private JSpinner Bg_textField;
	 private JSpinner textField_Nslices;
	 private JSpinner textField_PixelSize;
	 JLabel lbl_Slice;
	 private JSpinner textField_nFrames;
	 private JSpinner textField_Scale;
	 JLabel lblNf;
	 JLabel lbl_ProgMessage;
	public boolean isWindowClosed = false;
	public boolean isStop = false;
	int WidBufImg = 512;
	int HeiBufImg = 512;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ISMImageProcessorGUI frame = new ISMImageProcessorGUI();
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param ismControlGUI 
	 */
	public ISMImageProcessorGUI() {
		getContentPane().setEnabled(false);
		setTitle("The Third Institute of Physics-University of Göttingen");	
		Locale.setDefault(Locale.US);
				
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(-377, -51, 810, 881);
		getContentPane().setLayout(null);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("icon_img.png")));
		
		
		ism = new ISMprocessor(ISMImageProcessorGUI.this);
		fChooser = new JFileChooser();	
		
		
		textField_Ref = new JTextField();
		textField_Ref.setText("E:\\GWDG\\ISM\\data\\pos_blue_1.txt");//
		textField_Ref.setText("E:\\GWDG\\ISM\\data\\640_reference_251_M1_1_M2_3_44cycles_4pulse\\640_reference_251_M1_1_M2_3_44cycles_4pulse_MMStack_Pos0.ome.tif");
		textField_Ref.setText("C:\\Users\\CSDISM-user\\Desktop\\sample\\640_reference_251_M1_1_M2_3_44cycles_4pulse\\640_reference_251_M1_1_M2_3_44cycles_4pulse_MMStack_Pos0.ome.tif");
		textField_Ref.setText("D:\\GWDG\\ISM\\data\\meridian_reference_At488\\ISM_Ref_xy.txt");
		textField_Ref.setText("U:\\Sebastian\\Data\\170728 ISM ORCA\\ref_473nm_250_4pulse_later\\ref_473nm_250_4pulse_later_MMStack_Pos0.ome.tif");
		textField_Ref.setText("U:\\Sebastian\\Data\\170728 ISM ORCA\\ref_473nm_250_4pulse_later\\ISM_Ref_xy.txt");
		textField_Ref.setText("U:\\Sebastian\\Data\\180223 ISM\\ref405_Chroma_30c_250im_4p_25d_1\\ref405_Chroma_30c_250im_4p_25d_1_MMStack_Pos0.ome.txt");
		textField_Ref.setText("E:\\GWDG\\VS2013\\OpenCVtest\\OpenCVtest\\ref405_Chroma_30c_250im_4p_25d_1_MMStack_Pos0.omePOS_XY.txt");
		textField_Ref.setText("D:\\GWDG\\VS2013\\OpenCVtest\\OpenCVtest\\ref405_Chroma_30c_250im_4p_25d_1_MMStack_Pos0.ome.tif");
		
		textField_Ref.setBounds(184, 802, 419, 24);
		getContentPane().add(textField_Ref);
		textField_Ref.setColumns(10);
		
		
		JLabel lblRefImage = new JLabel("Reference:");
		lblRefImage.setBounds(112, 805, 91, 18);
		getContentPane().add(lblRefImage);
			
		
		textField_Img = new JTextField();
		
		textField_Img.setText("D:\\GWDG\\ISM\\data\\IHCs_p14_calretinin488_Vglut568_Otof647\\blue_1\\blue_1_MMStack_Pos0.ome.tif");
		textField_Img.setText("D:\\GWDG\\ISM\\data\\meridian_25um_1um_0.3um_31um_101slice_zStack\\meridian_25um_1um_0.3um_31um_101slice_zStack_MMStack_Pos0.ome.tif");
		textField_Img.setText("D:\\GWDG\\ISM\\data\\stairs_10um_ISMzStack\\stairs_10um_ISMzStack_MMStack_Pos0.ome.tif");
		//textField_Img.setText("D:\\GWDG\\ISM\\data\\170718 Robert cells\\EPC2_Phalloidin647_ISM\\EPC2_Phalloidin647_ISM_MMStack_Pos0.ome.tif");
		//textField_Img.setText("D:\\GWDG\\ISM\\data\\green_8\\green_8_MMStack_Pos0.ome.tif");
		textField_Img.setText("U:\\Sebastian\\Data\\170518 Christian\\IHCs_p14_tubulin580\\green_8\\green_8_MMStack_Pos0.ome.tif");
		textField_Img.setText("U:\\Sebastian\\Data\\170718 Robert cells\\reference_640_125_ORCA_full\\reference_640_125_ORCA_full_MMStack_Pos0.ome.tif");
		//U:\Sebastian\Data\170728 ISM ORCA\cross_125_4pulse\cross_125_4pulse_MMStack_Pos0.ome.tif
		textField_Img.setText("U:/Sebastian/Data/170728 ISM ORCA/cell_405nm_416LP_zStack_1/cell_405nm_416LP_zStack_1_MMStack_Pos0.ome.tif");
		textField_Img.setText("U:\\Sebastian\\Data\\170728 ISM ORCA\\cell_640nm_LP416_zStack_HQ\\cell_640nm_quadBP_zStack_2_MMStack_Pos0.ome.tif");
		
		textField_Img.setText("U:\\Sebastian\\Data\\180223 ISM\\ref405_Chroma_30c_250im_4p_25d_1\\ref405_Chroma_30c_250im_4p_25d_1_MMStack_Pos0.ome.tif");
		textField_Img.setText("D:\\GWDG\\VS2013\\OpenCVtest\\OpenCVtest\\ref405_Chroma_30c_250im_4p_25d_1_MMStack_Pos0.ome.tif");
		textField_Img.addInputMethodListener(new InputMethodListener() {
			public void caretPositionChanged(InputMethodEvent arg0) {
			}
			public void inputMethodTextChanged(InputMethodEvent arg0) {
				 
				
			}
		});
		textField_Img.setBounds(184, 765, 419, 24);
		getContentPane().add(textField_Img);
		textField_Img.setColumns(10);
		
		JLabel lblRawImage = new JLabel("The sample:");
		lblRawImage.setBounds(111, 770, 91, 18);
		getContentPane().add(lblRawImage);
		
		JButton btnSave = new JButton("Save as");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int returnVal = fChooser.showSaveDialog(ISMImageProcessorGUI.this);	
				 
	            if (returnVal == JFileChooser.APPROVE_OPTION) {
	                File file = fChooser.getSelectedFile();
	                
	                ism.saveImageAs(file.getAbsolutePath());
	               
	                    /*   
	                try {
	                	
	                	BufferedImage ismImg = ism.getISMimage();
	                	if(ismImg!=null){
	                		ImageIO.write(bufImage, "png", file);
	                	}else{
	                		 JOptionPane.showMessageDialog(ISMImageProcessorGUI.this, "ISM image is not available!");	                		
	                	}
	                	
	                	ImageIO.write(bufImage, "png", file);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}//
					*/
	                //This is where a real application would open the file.
	                System.out.println(file.getAbsolutePath());
	            }
			}
		});
		btnSave.setBounds(546, 650, 109, 27);
		getContentPane().add(btnSave);
		
		
		final JButton btnOpenRef = new JButton("Open...");//new ImageIcon("resource/open_folder.png")
		
		btnOpenRef.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int returnVal = fChooser.showOpenDialog(ISMImageProcessorGUI.this);
				File file = fChooser.getSelectedFile();
	            if (returnVal == JFileChooser.APPROVE_OPTION) {
	                //This is where a real application would open the file.
	            	textField_Ref.setText(file.getAbsolutePath());
	            }
				
			}
		});
		btnOpenRef.setBounds(605, 802, 81, 24);
		getContentPane().add(btnOpenRef);
		
		JButton btnOpenImg = new JButton("Open...");
		btnOpenImg.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fChooser.setMultiSelectionEnabled(true);
				int returnVal = fChooser.showOpenDialog(ISMImageProcessorGUI.this);
				 
	            if (returnVal == JFileChooser.APPROVE_OPTION) {
	                
	                File[] files = fChooser.getSelectedFiles();
	                int Nf = files.length;
	                List<String> Flist = new ArrayList<String>();
	                String str = "";
                	for(int i=0; i<Nf; i++){
                		String absPath = files[i].getAbsolutePath();
                		Flist.add(absPath);
                		str +=  str + absPath + "; ";
                		
                	}
	                	
	                //This is where a real application would open the file.
	                textField_Img.setText(str);
	                ism.sampleFlist = Flist;
	            }
	            
	            fChooser.setMultiSelectionEnabled(false);
			}
		});
		btnOpenImg.setBounds(605, 765, 81, 24);
		getContentPane().add(btnOpenImg);
		
		
		final JButton btnISMButton = new JButton("ISM");
		btnISMButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

			    isStop = false;
				bufImage = new BufferedImage(WidBufImg, HeiBufImg, BufferedImage.TYPE_USHORT_GRAY);
				ISMImageProcessorGUI.this.repaint();
				
				btnISMButton.setEnabled(false);
				lbl_ProgMessage.setText("");
				
				ism.PixelSize = (Float) textField_PixelSize.getValue();
				ism.FWHM = (Float) FWHM_textField.getValue();
				
				if(ism.FWHM<=ism.PixelSize){
					JOptionPane.showMessageDialog(ISMImageProcessorGUI.this, "FWHM should be larger than pixel size!");
					btnISMButton.setEnabled(true);
					return;
				}	
				
				double sigma = ism.FWHM/ism.PixelSize/2.355;
				ism.nPSFpixel = (float) ism.FWHM*2/ism.PixelSize; //(2*sigma*Math.sqrt(2*Math.log(1/0.05)));				
				
				ism.nSlice = (Integer) textField_Nslices.getValue();
				ism.nFrames =(Integer) textField_nFrames.getValue();
				ism.scale = (Integer) textField_Scale.getValue();
				final String ImgPath = textField_Img.getText();
				
				ism.background = (Integer) Bg_textField.getValue();
				ism.ProcessAlg = "ISM";
				
				final String RefPath = textField_Ref.getText().replace(";", "").trim();
				File RefFile = new File(RefPath);
				    
			    ism.RefDir = RefFile.getParent();
			    String[] strPath = ImgPath.split(";");
			    
			    List<String> ISMfilelist = new ArrayList<String>();
			    for(int i=0; i<strPath.length; i++){
			    	
			    	String str = strPath[i];
			    	str = str.trim();
			    	if(str!=null && !str.isEmpty()){
			    		ISMfilelist.add(str);
			    	}
			    	
			    }
			    ism.sampleFlist = ISMfilelist;
			    
			    Runnable runner = new Runnable()
			    {
			        public void run() {
					    progressBar.setVisible(true);
					    progressBar.setValue(0); 
					    lbl_Slice.setText("NSlice:");
					    
					    try{
					    	//bufImage = ism.ISMimageProcess(ImgPath, RefPath);
					    	ism.MulISMimageProcess(RefPath);
					    	
					    }catch(Exception e){
					    	btnISMButton.setEnabled(true);
					    }
						
						//ISMImageProcessorGUI.this.repaint();
						btnISMButton.setEnabled(true);
						//lbl_Slice.setText("Finished!");
						//progressBar.setVisible(false);
			        }
			    };
			    Thread t = new Thread(runner, "Code Executer");
			    t.start();
			    


			}
		});
		btnISMButton.setBounds(272, 650, 102, 27);
		getContentPane().add(btnISMButton);
		
		
		
		final JButton btnAverage = new JButton("Average");
		btnAverage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			    isStop = false;
				final String ImgPath = textField_Img.getText();
				File ImgFile = new File(ImgPath);
				final String RefPath = textField_Ref.getText().replace(";", "").trim();
				File RefFile = new File(RefPath);
			    ism.RefDir = RefFile.getParent();
			    ism.ImageDir = ImgFile.getParent();
				ism.nSlice = (Integer) textField_Nslices.getValue();
				ism.nFrames =(Integer) textField_nFrames.getValue();
				
			    String[] strPath = ImgPath.split(";");
			    
			    List<String> ISMfilelist = new ArrayList<String>();
			    for(int i=0; i<strPath.length; i++){
			    	
			    	String str = strPath[i];
			    	str = str.trim();
			    	if(str!=null && !str.isEmpty()){
			    		ISMfilelist.add(str);
			    	}
			    	
			    }
			    ism.sampleFlist = ISMfilelist;
				
			    btnAverage.setEnabled(false);
			    Runnable runner = new Runnable(){
			        public void run() {
						
						String filepath = textField_Img.getText();
						File file = new File(filepath);
						
						 progressBar.setVisible(true);
						 progressBar.setValue(0);
						 lbl_Slice.setText("");
						 //bufImage = ism.getAverage(filepath);
						 ism.ProcessAlg = "AVG";
						 try{
							 ism.MulISMimageProcess(null);
						 }catch(Exception e){
							 btnAverage.setEnabled(true);
							 return;
						 }
						 
						 //ISMImageProcessorGUI.this.repaint();
						 //lbl_Slice.setText("Finished!");
						 //progressBar.setVisible(false);
						 btnAverage.setEnabled(true);

			        }
			    };
			    Thread t = new Thread(runner, "Code Executer");
			    t.start();
				

			}
		});
		btnAverage.setBounds(137, 650, 102, 27);
		getContentPane().add(btnAverage);
		
		
		panel_ImageView = new JPanel();
		panel_ImageView.setBounds(76, 62, 645, 552);
		getContentPane().add(panel_ImageView);
		panel_ImageView.setBorder(new TitledBorder(null, "Image View", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_ImageView.setLayout(null);
		

		//image display panel
		drawpanel.setBounds(10, 15, panel_ImageView.getWidth()-20, panel_ImageView.getHeight()-25);
		ISMImageProcessorGUI.this.getContentPane().add(drawpanel);
		drawpanel.setBackground (Color.BLACK);
		panel_ImageView.add(drawpanel);
		WidBufImg = drawpanel.getWidth();
		HeiBufImg = drawpanel.getHeight();
		
		progressBar = new JProgressBar();
		progressBar.setMaximum(250);
		progressBar.setBounds(137, 625, 518, 14);
		getContentPane().add(progressBar);
		
		FWHM_textField = new JSpinner();
		FWHM_textField.setModel(new SpinnerNumberModel(new Float(400), new Float(1), null, new Float(1)));
		FWHM_textField.setBounds(350, 707, 60, 20);
		getContentPane().add(FWHM_textField);
		
		JLabel lbl_FWHM = new JLabel("FWHM(nm):");
		lbl_FWHM.setBounds(275, 708, 80, 18);
		getContentPane().add(lbl_FWHM);
		
		Bg_textField = new JSpinner();
		Bg_textField.setModel(new SpinnerNumberModel(new Integer(100), new Integer(0), null, new Integer(100)));
		Bg_textField.setBounds(512, 706, 63, 20);
		getContentPane().add(Bg_textField);

		JLabel lblBackgroud = new JLabel("Background:");
		lblBackgroud.setBounds(431, 708, 91, 18);
		getContentPane().add(lblBackgroud);

		JLabel lblIsmImageProcessing = new JLabel("ISM Image Processor");
		lblIsmImageProcessing.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblIsmImageProcessing.setHorizontalAlignment(SwingConstants.CENTER);
		lblIsmImageProcessing.setBounds(164, 22, 448, 29);
		getContentPane().add(lblIsmImageProcessing);
		
		textField_Nslices = new JSpinner();
		textField_Nslices.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		textField_Nslices.setBounds(196, 734, 65, 20);
		getContentPane().add(textField_Nslices);
		
		JLabel lbl_Slices = new JLabel("Slice Number:");
		lbl_Slices.setBounds(111, 737, 84, 14);
		getContentPane().add(lbl_Slices);
		
		final JButton btnLocalizer = new JButton("Localize");
		btnLocalizer.setEnabled(false);
		btnLocalizer.setVisible(false);
		btnLocalizer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				isStop = false;
				lbl_ProgMessage.setText("");			
				ism.PixelSize = (Float) textField_PixelSize.getValue();
				ism.FWHM = (Float) FWHM_textField.getValue();
				double sigma = ism.FWHM/ism.PixelSize/2.355;
				if(ism.FWHM<=ism.PixelSize){
					JOptionPane.showMessageDialog(ISMImageProcessorGUI.this, "FWHM should be larger than pixel size!");
					return;
				}				

				ism.nPSFpixel = (float) (2*sigma*Math.sqrt(2*Math.log(1/0.1)));
				ism.nSlice = (Integer) textField_Nslices.getValue();
				ism.nFrames =(Integer) textField_nFrames.getValue();
				ism.scale = (Integer) textField_Scale.getValue();
				
				bufImage = new BufferedImage(WidBufImg, HeiBufImg, BufferedImage.TYPE_USHORT_GRAY);
				ISMImageProcessorGUI.this.repaint();
				
				
				final String ImgPath = textField_Img.getText();
				File ImgFile = new File(ImgPath);
				final String RefPath = textField_Ref.getText().replace(";", "").trim();			
				File RefFile = new File(RefPath);
				
				ism.background = (Integer) Bg_textField.getValue();
				
			    if(!RefFile.exists() || RefFile.isDirectory()){
				     JOptionPane.showMessageDialog(ISMImageProcessorGUI.this, "Reference image file not found: " + RefPath);
				     btnLocalizer.setEnabled(true);
				     return;
			    }
			    btnLocalizer.setEnabled(false);
			    ism.RefDir = RefFile.getParent();
			    //ism.ImageDir = ImgFile.getParent();
			    Runnable runner = new Runnable()
			    {
			        public void run() {
					    progressBar.setVisible(true);
					    progressBar.setValue(0); 
					    lbl_Slice.setText("NSlice:");
					    bufImage = null;
					    
					    try{
					    	ism.imStackLocalize(RefPath);
					    }catch(Exception e){
					    	btnLocalizer.setEnabled(true);
					    	return;
					    }
						
						ISMImageProcessorGUI.this.repaint();	
						//lbl_Slice.setText("Finished!");
						btnLocalizer.setEnabled(true);
						//progressBar.setVisible(false);
			        }
			    };
			    Thread t = new Thread(runner, "Code Executer");
			    t.start();			
				
			}
		});
		btnLocalizer.setBounds(151, 630, 60, 27);
		getContentPane().add(btnLocalizer);
		
		lbl_Slice = new JLabel("NSlice:");
		lbl_Slice.setBounds(595, 709, 51, 14);
		getContentPane().add(lbl_Slice);
		
		textField_nFrames = new JSpinner();
		textField_nFrames.setModel(new SpinnerNumberModel(new Integer(250), new Integer(0), null, new Integer(1)));
		textField_nFrames.setBounds(350, 734, 60, 20);
		getContentPane().add(textField_nFrames);
		
		JLabel lblnFrames = new JLabel("nFrames/Slice:");
		lblnFrames.setBounds(265, 737, 94, 14);
		getContentPane().add(lblnFrames);
		
		textField_Scale = new JSpinner();
		textField_Scale.setModel(new SpinnerNumberModel(4, 1, 20, 1));
		textField_Scale.setBounds(512, 734, 63, 20);
		getContentPane().add(textField_Scale);
		
		JLabel lblScale = new JLabel("Upsample scale:");
		lblScale.setBounds(415, 736, 102, 18);
		getContentPane().add(lblScale);
		
		JLabel lblNframe = new JLabel("NFrame:");
		lblNframe.setBounds(587, 735, 51, 18);
		getContentPane().add(lblNframe);
		
		lblNf = new JLabel("");
		lblNf.setBounds(642, 735, 51, 18);
		getContentPane().add(lblNf);
		
		lbl_ProgMessage = new JLabel("");
		lbl_ProgMessage.setForeground(new Color(0, 255, 0));
		lbl_ProgMessage.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_ProgMessage.setBackground(new Color(245, 245, 245));
		lbl_ProgMessage.setFont(UIManager.getFont("Label.font"));
		lbl_ProgMessage.setBounds(231, 683, 349, 14);
		//lbl_ProgMessage.setHorizontalAlignment(LEADING);
		getContentPane().add(lbl_ProgMessage);
		
		JButton btnStop = new JButton("Stop");
		btnStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				isStop = true;
				btnISMButton.setEnabled(true);
			}
		});
		btnStop.setBounds(410, 650, 102, 27);
		getContentPane().add(btnStop);
		
		textField_PixelSize = new JSpinner();
		textField_PixelSize.setModel(new SpinnerNumberModel(new Float(100), new Float(1), null, new Float(1)));
		textField_PixelSize.setBounds(195, 705, 65, 20);
		getContentPane().add(textField_PixelSize);
		
		JLabel lblPixelsize = new JLabel("Pixel size(nm):");
		lblPixelsize.setBounds(111, 708, 91, 14);
		getContentPane().add(lblPixelsize);
		progressBar.setVisible(false);
		
		this.addWindowListener(new java.awt.event.WindowAdapter(){
	       

			public void windowClosing(WindowEvent winEvt) {
				isWindowClosed = true;
	        	//System.exit(1);
	        	//System.out.println("Window closed!");
	        }
	    });
		
		
	}
	
	
	class Drawpanel extends JPanel{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		//private BufferedImage image;
	
		@Override
		protected void paintComponent(Graphics g){
			super.paintComponent(g);
			if(bufImage!=null){
				g.drawImage(bufImage, 0, 0, getWidth(), getHeight(),null);
			}
			
		}
		
	}	
}
