----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:19:52 03/14/2017 
-- Design Name: 
-- Module Name:    ISMcoreWrapper - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ISMcoreWrapper is
    Port ( clk : in  STD_LOGIC;
           signal_in : in  STD_LOGIC;
           enable : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           M : in  STD_LOGIC_VECTOR (15 downto 0);
		   M1 : in  STD_LOGIC_VECTOR (15 downto 0);
		   M2 : in  STD_LOGIC_VECTOR (15 downto 0);
           N : in  STD_LOGIC_VECTOR (15 downto 0);
		   Wp: in STD_LOGIC_VECTOR (15 downto 0);
		     nextdelay : in  STD_LOGIC_VECTOR (15 downto 0);
		     Nlaserpuls : in  STD_LOGIC_VECTOR (15 downto 0);
           finished : out  STD_LOGIC;
           campulse : out  STD_LOGIC;
		   count0 : out  STD_LOGIC_VECTOR (22 downto 0);
		   count1 : out  STD_LOGIC_VECTOR (22 downto 0);
           images : out  STD_LOGIC_VECTOR (15 downto 0);
           trig : out  STD_LOGIC);
			  
end ISMcoreWrapper;

architecture Behavioral of ISMcoreWrapper is

component ISMcore

    Port ( clk : in  STD_LOGIC;
           signal_in : in  STD_LOGIC;
           enable : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           M : in  STD_LOGIC_VECTOR (15 downto 0);
		   M1: in  STD_LOGIC_VECTOR (15 downto 0);
		   M2: in  STD_LOGIC_VECTOR (15 downto 0);
           N : in  STD_LOGIC_VECTOR (15 downto 0);
		   Wp: in STD_LOGIC_VECTOR (15 downto 0);
		     nextdelay : in  STD_LOGIC_VECTOR (15 downto 0);
		     Nlaserpuls : in  STD_LOGIC_VECTOR (15 downto 0);
           finished : out  STD_LOGIC;
           campulse : out  STD_LOGIC;
		   count0 : out  STD_LOGIC_VECTOR (22 downto 0);
		   count1 : out  STD_LOGIC_VECTOR (22 downto 0);
           images : out  STD_LOGIC_VECTOR (15 downto 0);
           trig : out  STD_LOGIC);
			  
end component;			  


begin

MyISMcore: ISMcore

port map(
			  clk => clk,
           signal_in => signal_in,
           enable => enable,
           rst => rst,
           M => M,
		   M1 => M1,
		   M2 => M2,
           N => N,
		   Wp => Wp,
		     nextdelay => nextdelay,
		     Nlaserpuls => Nlaserpuls,
           finished => finished,
           campulse => campulse,
           count0 => count0,
		   count1 => count1,
           images => images,
           trig => trig);
			  
end Behavioral;

