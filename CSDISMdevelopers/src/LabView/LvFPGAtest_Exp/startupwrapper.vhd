----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:19:52 03/14/2017 
-- Design Name: 
-- Module Name:    ISMcoreWrapper - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity startupwrapper is
    Port ( clk : in  STD_LOGIC;
			en : in  STD_LOGIC;
           uprise : in  STD_LOGIC;
           enable : out  STD_LOGIC;
           rst : out  STD_LOGIC;
		   z : out  STD_LOGIC);
			  
end startupwrapper;

architecture Behavioral of startupwrapper is

component startup

    Port ( clk : in  STD_LOGIC;
		   en : in  STD_LOGIC;
            uprise : in  STD_LOGIC;
           enable : out  STD_LOGIC;
           rst : out  STD_LOGIC;
		   z : out  STD_LOGIC);
			  
end component;			  


begin

Mystartup: startup

port map(
			  clk => clk,
				en => en,
           uprise => uprise,
           enable => enable,
           rst => rst,
		   z =>z);
			  
end Behavioral;

