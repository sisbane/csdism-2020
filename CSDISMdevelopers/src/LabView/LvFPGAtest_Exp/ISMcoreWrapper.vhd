----------------------------------------------------------------------------------
-- FPGA program for SDC-ISM MicroManager Plugin: A open source software for Spinning Disk Confocal-Image Scanning microscopy image acquisition
--
--     Copyright (C) 2019  Shun Qin, shun.qin@phys.uni-goettingen.de
-- 
--     This program is free software: you can redistribute it and/or modify
--     it under the terms of the GNU General Public License as published by
--     the Free Software Foundation, either version 3 of the License, or
--     (at your option) any later version.
-- 
--     This program is distributed in the hope that it will be useful,
--     but WITHOUT ANY WARRANTY; without even the implied warranty of
--     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--     GNU General Public License for more details.
-- 
--     You should have received a copy of the GNU General Public License
--     along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--     We expect you would cite our article, to which this source code project attach.

--     Contact:
--     Insitute: III Physics Institute, University of Goettingen
--     Author: Shun Qin
--     Contact: shun.qin@phys.uni-goettingen.de
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;



entity ISMcoreWrapper is
    Port ( clk : in  STD_LOGIC;
           signal_in : in  STD_LOGIC;
           enable : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           M : in  STD_LOGIC_VECTOR (15 downto 0);
		   M1 : in  STD_LOGIC_VECTOR (15 downto 0);
		   M2 : in  STD_LOGIC_VECTOR (15 downto 0);
           N : in  STD_LOGIC_VECTOR (15 downto 0);
		   Wp: in STD_LOGIC_VECTOR (15 downto 0);
		     nextdelay : in  STD_LOGIC_VECTOR (15 downto 0);
		     Nlaserpuls : in  STD_LOGIC_VECTOR (15 downto 0);
           finished : out  STD_LOGIC;
           campulse : out  STD_LOGIC;
		   count0 : out  STD_LOGIC_VECTOR (22 downto 0);
		   count1 : out  STD_LOGIC_VECTOR (22 downto 0);
           images : out  STD_LOGIC_VECTOR (15 downto 0);
           trig : out  STD_LOGIC);
			  
end ISMcoreWrapper;

architecture Behavioral of ISMcoreWrapper is

component ISMcore

    Port ( clk : in  STD_LOGIC;
           signal_in : in  STD_LOGIC;
           enable : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           M : in  STD_LOGIC_VECTOR (15 downto 0);
		   M1: in  STD_LOGIC_VECTOR (15 downto 0);
		   M2: in  STD_LOGIC_VECTOR (15 downto 0);
           N : in  STD_LOGIC_VECTOR (15 downto 0);
		   Wp: in STD_LOGIC_VECTOR (15 downto 0);
		     nextdelay : in  STD_LOGIC_VECTOR (15 downto 0);
		     Nlaserpuls : in  STD_LOGIC_VECTOR (15 downto 0);
           finished : out  STD_LOGIC;
           campulse : out  STD_LOGIC;
		   count0 : out  STD_LOGIC_VECTOR (22 downto 0);
		   count1 : out  STD_LOGIC_VECTOR (22 downto 0);
           images : out  STD_LOGIC_VECTOR (15 downto 0);
           trig : out  STD_LOGIC);
			  
end component;			  


begin

MyISMcore: ISMcore

port map(
			  clk => clk,
           signal_in => signal_in,
           enable => enable,
           rst => rst,
           M => M,
		   M1 => M1,
		   M2 => M2,
           N => N,
		   Wp => Wp,
		     nextdelay => nextdelay,
		     Nlaserpuls => Nlaserpuls,
           finished => finished,
           campulse => campulse,
           count0 => count0,
		   count1 => count1,
           images => images,
           trig => trig);
			  
end Behavioral;

