/***************************************************************************************************************************************

// FPGA program for SDC-ISM MicroManager Plugin: A open source software for Spinning Disk Confocal-Image Scanning microscopy image acquisition
//
//     Copyright (C) 2019  Shun Qin, shun.qin@phys.uni-goettingen.de
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//     We expect you would cite our article, to which this source code project attach.

//     Contact:
//     Insitute: III Physics Institute, University of Goettingen
//     Author: Shun Qin
//     Contact: shun.qin@phys.uni-goettingen.de

*******************************************************************************************************************************************/

module ISMcore(clk, signal_in, enable, rst, M, M1, M2, N, Wp, nextdelay, Nlaserpuls, finished, campulse, count0, count1, images, trig);

parameter L1 = 23; // 24 bits counter
parameter L2 = 20; // 21 bits registers
parameter L3 = 15;
//parameter M1 = 2;
//parameter M2 = 2;
input[15:0] M1;
input[15:0] M2;

input [15:0] Wp;
input [15:0] M; //number of cycle of disk pattern spinning for camera exposure time
input [15:0] N; //number of images need to be taken
input [15:0] Nlaserpuls;

input enable;
input clk;    //system clock in 40MHz
input signal_in; //input signal from disk sensor 
input rst;    


output finished; //signal indicate acquisition have been finished
//reg clk_out;

output campulse; //camera trigger signal  
reg campulse;
reg [15:0] delay, delay2;
input [15:0] nextdelay;
//output [15:0] delay;



output images;
reg [15:0] images; //number of image taken at the moment

//output [7:0] cdisk;
reg [15:0]  cdisk; //counter of rising edge of disk signal

reg on;
reg flag1;

output[L1:0] count0, count1;

reg [L2+L1+1:0] quotient, dt;
reg [L1:0] count0, count1, count2, count3, count4, count5, count6, count7, count8, count9, count10, count11, count12, count13, count14, count15, count16, count17, sum1, sum2, sum3, sum4;
reg [L1+4:0] count;

reg signed [L2:0] c, c0, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12;
reg s0, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13, s14, s15, sf;

reg en;
reg [15:0] num; //number of cycle of disk pulse

output trig;
reg trig;

reg r0;
reg r1;
wire rising;

reg r3, r4;
reg rise;

reg[L2:0] laserps;  
reg[15:0] used;

assign finished = en;
//assign trig = enable && count1>20 && count1 < 25 && !en;
assign rising = enable && !r0 && r1;
//assign campulse = 8'b0;
//campulse = cdisk;
reg[15:0] ninp;
reg[31:0] y0, y1;
reg[31:0] left, right0, right1, right2, right3, right4, right5, right6, right7, right8, right9, right10, right11, right12, right13, right14, right15, W, cninp, left1;
reg shift, ready;


					
reg u0, u1, u2, u3, u4, u5, u6, u7, u8, u9, u10, u11, u12, u13, u14, u15, v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15;
reg [15:0] LPcount0, LPcount1,  LPcount2, LPcount3, LPcount4, LPcount5, LPcount6, LPcount7, LPcount8, LPcount9, LPcount10, LPcount11, LPcount12, LPcount13, LPcount14, LPcount15;
reg[7:0] RSTcount;
reg trig1, trig2, trig3, trig4, trig5, trig6, trig7, trig8, trig9, trig10, trig11, trig12, trig13, trig14, trig15;
//assign trig1 = laser1 > 30 && laser1 < 35 && (!rst && !en);
//assign campulse = (cdisk == 0);
  
// power-on-reset 
//r0 = 1; r1 =0; count0 = 0;
// before start, count0, r0 have to be in right state, for this delay for at least one cycle of signal is must and it must be in reset state during this duration
  
reg[15:0] Cnt; 
reg trigout0, trigout1; 
  
  
  
  
always@(negedge clk)

if(enable) begin
	/*
	if(!rst && !en)	begin
	  
	   laser1 <= laser1 + 1'b1; 	  
	  
	end
	
	if(!r0 && r1)
	  rising = 1;
	else
	  rising = 0;
	*/
	r0 <= r1;
	if(signal_in)
		r1 <= 1;
	else
		r1 <= 0;
   
    
end



always@(posedge clk)



if(enable) begin
	r3 = r4;
	if(signal_in)
		r4 = 1;
	else
		r4 = 0;
	
	if(!r3 && r4)
		rise = 1;
	else 
		rise = 0;

		
		
	
		

	if(rise) begin
	
	
		/* if(rst && RSTcount > 0)  begin
			RSTcount = 0;
		end

		else begin
			RSTcount = 1;
		
		end	
	
		if(rst && RSTcount==0) begin
			count17 = 0;
			count16 = 0;
			count15 = 0;
			count14 = 0;
			count13 = 0;
			count12 = 0;
			count11 = 0;
			count10 = 0;
			count9 = 0;
			count8 = 0;
			count7 = 0;
			count6 = 0;
			count5 = 0;
			count4 = 0;
			count3 = 0;
			count2 = 0;
			//count1 = 0;
			count = 0;
			//count0 = 0;
		
		end
		else */
		begin
		
			sum1 =  count1  + count2 + count3 + count4; 
			sum2 =  count5  + count6 + count7 + count8; 
			//sum3 =  count9  + count10 + count11 + count12;
			//sum4 =  count13  + count14 + count15 + count16;
			count = sum1+ sum2 ; //+ sum3+ sum4
			
			count0 = count[L1+4:3];

/* 			count17 = count16;
			count16 = count15;
			count15 = count14;
			count14 = count13;
			count13 = count12;
			count12 = count11;
			count11 = count10;
			count10 = count9;
			count9 = count8;*/
			count8 = count7;
			count7 = count6;
			count6 = count5;
			count5 = count4; 
			count4 = count3;
			count3 = count2;
			count2 = count1;
			

			count1 = 0;
		   end
	/*
 		//clear counter
		count5 = count4;
		count4 = count3;
		count3 = count2;
		count2 = count1;		
		//count0 = count1;	
		count0 = (count5 + count4 + count3 + count2)>>2;
		count1 = 1;  */ 
	end
	else		
		count1 = count1 + 1'b1;	//ticks from the rising of disk's sensor output signal	



	if(!rst) begin  
		if(!en) begin //en: signal of finishing

			
			
		
			if(rise) begin
			
				//if(cdisk == M)
					//cdisk = 8'b0; 
				//else  
			
			
					if(cdisk == M+M2-1)  begin //M: number of cycle for one image, M1: Ncycle before laser trigger, M2: delay after laser trigger and should be >=1
					
						images = num;   //Count the number of image taken  
						
						cdisk = 16'b0; 
												
						   
						//if(num < N)
							//campulse = 1;
						//else begin    // finished
						if(num>=N) begin
							en = 1; 
							on = 0;   // indicate pulsing is on going from 1th to Nth image
							//trig = 0;
							campulse = 0;							
						end
						
						
						if(images == 0) begin
							on = 1;
							delay = 0;
						end
						else begin
							
							//delay = delay + dt[2*L3 + 1 : L3 + 1]; //dt = Tn/(Np*Ni)
							delay = delay + 1;
						end
							
						num = num + 1'b1;  	
						
					end
					else  begin
							
					   //campulse = 0;
					   //delay = 0 ;
					   cdisk = cdisk + 1;	
					end
					

						
					/*	 
					if(num > N + 1) begin				
					   en = 1; 
						on = 0;
						trig = 0;
						campulse = 0;
					end
					

				if(cdisk == M)
					cdisk = 0;
				else
					cdisk = cdisk + 1;	
				*/	
			end		 		


			
			if(num<N && cdisk == (M+M2-1) && count0>M1 && (count1==count0-M1) || cdisk == 0 && rise && num<=N) begin	//M1==0 && 				       
				   campulse = 1;					
			end		
			
			// to produce laser pulses
			if(on) begin//  
			
				if(cdisk == M && count1==Wp)  begin
					campulse = 0;
				end
				

				
			
				if(cdisk >= 0 && cdisk < M) begin
					/*
					// Only available for 4 laser pulse output
					c = count1[L2:0];
					count2 = count0 - 8; 
					delay2 = delay + 8; //delay a little so as to offer camera sufficient activation time
					c0 = delay2;
					c1 = count2[22:2] + delay2;
					c2 = count2[21:1] + delay2;	
					c3 = count2[21:1] + count2[22:2] + delay2;
					 
					s0 = (c > c0) && (c <= c0 + Wp);//0T
					s1 = (c > c1) && (c <= c1 + Wp);//1/4T
					s2 = (c > c2) && (c <= c2 + Wp);//2/4T
					s3 = (c > c3) && (c <= c3 + Wp);//3/4T
					
					trig = s0 || s1 || s2 || s3; //laser trigger signal
				
					trig = (count1[L2:0] > delay) && (count1[L2:0] <= delay + Wp) || 
					(count1[L2:0] > count0[22:2] + delay) && (count1[L2:0] <= count0[22:2] + delay + Wp) || 
					(count1[L2:0] > count0[21:1] + delay) && (count1[L2:0] <= count0[21:1] + delay + Wp) || 
					(count1[L2:0] > count0[21:1] + count0[22:2] + delay) && (count1[L2:0] <= count0[21:1] + count0[22:2] + delay + Wp);
					*/
					if(rise) begin

						
						//x = n*N + m;
						y0 = count0*N;
						y1 = count0*delay;
						cninp = y0*Nlaserpuls; //c0*Ni*Np
						
						right0 = y1;
						right1 = y0 + y1;
						right2 = (y0<<1) + y1;
						right3 = (y0<<1) + (y0 + y1);//right1 + right2 - y1;
						right4 = (y0<<2) + y1;
						right5 = (y0<<2) + (y0 + y1);
						right6 = (y0<<2) + (y0<<1) + y1;
						right7 = (y0<<3) + (y1 - y0);
						right8 = (y0<<3) + y1;						
						right9 = (y0<<3) + (y0 + y1);
						right10 = (y0<<3) + (y0<<1) + y1;
						right11 = (y0<<3) + (y0<<2) + (y1 - y0);
						right12 = (y0<<3) + (y0<<2) + y1;
						right13 = (y0<<3) + (y0<<2) + (y0 + y1);
						right14 = (y0<<4) - (y0<<1) + y1;
						right15 = (y0<<4) + (y1 - y0);
						//right16 = (y0<<4) + y1;
						
					end	
						
					left = count1[L2:0]*ninp;	
			
					/*
					shift = left < (count0<<4);
					if((cdisk == M1) && shift || (cdisk == M-M2) && ~shift)	
						ready = 0;						
					else
						ready = 1;				 
					*/	 
								

					//left1 = left + cninp;
					//s0  = (left > right0 && left <= right0 + W || left > right0 - cninp && left <= right0 + W - cninp) && used[0];
					//trig = left > right0 && left <= right0 + W;
					
					// generate the first laser pulse
					u0 = s0; 
					s0 = left > right0;
					
					if(!u0 && s0) begin
						
						v0 = 1;
						//LPcount0 = 240;
					
					end
					

					
					
					// generate the second laser pulse
					u1 = s1; 
					s1 = left > right1;
					
					if(!u1 && s1) begin
						
						v1 = 1;
						//LPcount1 = 240;
					
					end
					
					

					
					// generate the 3th laser pulse
					u2 = s2; 
					s2 = left > right2;
					
					if(!u2 && s2 && Nlaserpuls>=3 ) begin
						
						v2 = 1;
						//LPcount2 = 240;
					
					end
					
				
					
					
					// generate the 4th laser pulse
					u3 = s3; 
					s3 = left > right3;
					
					if(!u3 && s3 &&  Nlaserpuls>=4) begin
						
						v3 = 1;
						//LPcount3 = 240;
					
					end
					
				
					
					
					// generate the 5th laser pulse
					u4 = s4; 
					s4 = left > right4;
					
					if(!u4 && s4 && Nlaserpuls>=5) begin
						
						v4 = 1;
					
					end
					





					// generate the 6th laser pulse
					u5 = s5; 
					s5 = left > right5;
					
					if(!u5 && s5 && Nlaserpuls>=6) begin
						
						v5 = 1;
					
					end
					

					// generate the 7th laser pulse
					u6 = s6; 
					s6 = left > right6;
					
					if(!u6 && s6 &&  Nlaserpuls>=7) begin
						
						v6 = 1;
					
					end
					
					
					
					// generate the 8th laser pulse
					u7 = s7; 
					s7 = left > right7;		
					if(!u7 && s7 && Nlaserpuls>=8) begin
						
						v7 = 1;
					
					end					
					
					
					// generate the 9th laser pulse
					u8 = s8; 
					s8 = left > right8;		
					if(!u8 && s8 && Nlaserpuls>=9) begin
						
						v8 = 1;
					
					end	
					

					// generate the 10th laser pulse
					u9 = s9; 
					s9 = left > right9;		
					if(!u9 && s9 && Nlaserpuls>=10) begin
						
						v9 = 1;
					
					end	


					// generate the 11th laser pulse
					u10 = s10; 
					s10 = left > right10;		
					if(!u10 && s10 && Nlaserpuls>=11) begin
						
						v10 = 1;
					
					end	
					
					
					// generate the 12th laser pulse
					u11 = s11; 
					s11 = left > right11;		
					if(!u11 && s11 && Nlaserpuls>=12) begin
						
						v11 = 1;
					
					end						
					
					
					
					
					// generate the 13th laser pulse
					u12 = s12; 
					s12 = left > right12;		
					if(!u12 && s12 && Nlaserpuls>=13) begin
						
						v12 = 1;
					
					end						
					
					
	

					// generate the 14th laser pulse
					u13 = s13; 
					s13 = left > right13;		
					if(!u13 && s13 && Nlaserpuls>=14) begin
						
						v13 = 1;
					
					end	



					// generate the 15th laser pulse
					u14 = s14; 
					s14 = left > right14;		
					if(!u14 && s14 && Nlaserpuls>=15) begin
						
						v14 = 1;
					
					end	
	
	
	
					// generate the 16th laser pulse
					u15 = s15; 
					s15 = left > right15;		
					if(!u15 && s15 && Nlaserpuls>=16) begin
						
						v15 = 1;
					
					end		
	
	
	
	
	
					
					if(images == N)
						sf = count1[L2:0] >0 && left <= count0;	
					else
						sf = 0;

					
				end  //  
				

			
				if(v0) begin
				
					LPcount0 = LPcount0 + 1;
				
				end
				
				if(LPcount0>Wp) begin
				
					v0 = 0;
					LPcount0 = 0;
				
				end	

			
				
				if(v1) begin
				
					LPcount1 = LPcount1 + 1;
				
				end
				
				if(LPcount1>Wp) begin
				
					v1 = 0;
					LPcount1 = 0;
				
				end					
				
				
				

				if(v2) begin
				
					LPcount2 = LPcount2 + 1;
				
				end
				
				if(LPcount2>Wp) begin
				
					v2 = 0;
					LPcount2 = 0;
				
				end	

	
	
				if(v3) begin
				
					LPcount3 = LPcount3 + 1;
				
				end
				
				if(LPcount3>Wp) begin
				
					v3 = 0;
					LPcount3 = 0;
				
				end	
	
	
	
	
	
				if(v4) begin
				
					LPcount4 = LPcount4 + 1;
				
				end
				
				if(LPcount4>Wp) begin
				
					v4 = 0;
					LPcount4 = 0;
				
				end	


				if(v5) begin
				
					LPcount5 = LPcount5 + 1;
				
				end
				
				if(LPcount5>Wp) begin
				
					v5 = 0;
					LPcount5 = 0;
				
				end	




	
				
				if(v6) begin
				
					LPcount6 = LPcount6 + 1;
				
				end
				
				if(LPcount6>Wp) begin
				
					v6 = 0;
					LPcount6 = 0;
				
				end					
				
				

				//
				if(v7) begin
				
					LPcount7 = LPcount7 + 1;
				
				end
				
				if(LPcount7>Wp) begin
				
					v7 = 0;
					LPcount7 = 0;
				
				end			




				//
				if(v8) begin
				
					LPcount8 = LPcount8 + 1;
				
				end
				
				if(LPcount8>Wp) begin
				
					v8 = 0;
					LPcount8 = 0;
				
				end		


				//
				if(v9) begin
				
					LPcount9 = LPcount9 + 1;
				
				end
				
				if(LPcount9>Wp) begin
				
					v9 = 0;
					LPcount9 = 0;
				
				end						
			
			
			
				//
				if(v10) begin
				
					LPcount10 = LPcount10 + 1;
				
				end
				
				if(LPcount10>Wp) begin
				
					v10 = 0;
					LPcount10 = 0;
				
				end					
			
			
				//
				if(v11) begin
				
					LPcount11 = LPcount11 + 1;
				
				end
				
				if(LPcount11>Wp) begin
				
					v11 = 0;
					LPcount11 = 0;
				
				end		
			
			
				//
				if(v12) begin
				
					LPcount12 = LPcount12 + 1;
				
				end
				
				if(LPcount12>Wp) begin
				
					v12 = 0;
					LPcount12 = 0;
				
				end		
			
			
			
				// counter for no. 14 laser pulse
				if(v13) begin
				
					LPcount13 = LPcount13 + 1;
				
				end
				
				if(LPcount13>Wp) begin
				
					v13 = 0;
					LPcount13 = 0;
				
				end		
			
				// counter for no. 15 laser pulse
				if(v14) begin
				
					LPcount14 = LPcount14 + 1;
				
				end
				
				if(LPcount14>Wp) begin
				
					v14 = 0;
					LPcount14 = 0;
				
				end		
			
			
			
				// counter for no. 16 laser pulse
				if(v15) begin
				
					LPcount15 = LPcount15 + 1;
				
				end
				
				if(LPcount15>Wp) begin
				
					v15 = 0;
					LPcount15 = 0;
				
				end		
			
			
			
			
			
				//trig = (LPcount0>0 && LPcount0 <= Wp) || (LPcount1>0 && LPcount1 <= Wp) || (LPcount2>0 && LPcount2 <= Wp) ||(LPcount3>0 && LPcount3 <= Wp) ||(LPcount4>0 && LPcount4 <= Wp) ||(LPcount5>0 && LPcount5 <= Wp) ||(LPcount6>0 && LPcount6 <= Wp)  ||(LPcount7>0 && LPcount7 <= Wp);

				
				trig = v0 || v1 || v2 || v3 || v4 || v5 || v6 || v7 || v8 || v9 || v10 || v11 || v12 || v13 || v14 || v15;
				
/* 				trig1 = LPcount0>0 && LPcount0 <= Wp;
				trig2 = LPcount1>0 && LPcount1 <= Wp;
				trig3 = LPcount2>0 && LPcount2 <= Wp && Nlaserpuls>=3;
				trig4 = LPcount3>0 && LPcount3 <= Wp && Nlaserpuls>=4;
				trig5 = LPcount4>0 && LPcount4 <= Wp && Nlaserpuls>=5;
				trig6 = LPcount5>0 && LPcount5 <= Wp && Nlaserpuls>=6;
				trig7 = LPcount6>0 && LPcount6 <= Wp && Nlaserpuls>=7;
				trig8 = LPcount7>0 && LPcount7 <= Wp && Nlaserpuls>=8;
				
				//trig = trig1;
				
				trig = trig8 || trig7 || trig6 || trig5 || trig4 || trig3 || trig2 || trig1; */
				
				/* case(Nlaserpuls)

	
					 1: begin		
							
							trig = trig1;
							//trig = s0;
						
						end
	
					 2: begin 
							
						   //trig = trig2; 
						   trig = trig2 || trig1;
						   //trig = s1;

						end
						
						
					 3:	begin 
							
						    //trig = trig3;
							trig = trig3 || trig2 || trig1;
							//trig = s2;
						end
						
					 5:	begin 
							//trig = trig5;
							trig = trig5 || trig4 || trig3 || trig2 || trig1;
							//trig = s4;

						end
						
					 6:	begin 
							//trig = trig1;
							trig = trig6 || trig5 || trig4 || trig3 || trig2 || trig1;
							//trig = s5;

						end
					 7:	begin 
							//trig = trig7;
							trig = trig7 || trig6 || trig5 || trig4 || trig3 || trig2 || trig1;
							//trig = s6;

						end		
					 8:	begin 
							//trig = trig8;
							trig = trig8 || trig7 || trig6 || trig5 || trig4 || trig3 || trig2 || trig1;
							//trig = s7;

						end	
						
						
					default: 
						begin 
							//trig = trig4;
							trig = trig4 || trig3 || trig2 || trig1;
						end	
						
						
						
				endcase */
				
				//trigout0 = trigout1;
				//trigout1 = trig8 || trig7 || trig6 || trig5 || trig4 || trig3 || trig2 || trig1;
				
				
			end
			

			


		end


		
		
	end
	
	else begin  	 
  //reset all registers
		



		
	  campulse = 0;
	  cdisk = (M + M2) - 2;
	  images = 0;
	  en = 0; 
	  num = 0;
	  //trig1 = 0; 
	  
	  c = 0; 
	  

		
		trig1 = 0;
		trig2 = 0;
		trig3 = 0;
		trig4 = 0;
		trig5 = 0;
		trig6 = 0;
		trig7 = 0;
		trig8 = 0;
		trig9 = 0;
		trig10 = 0;
		trig11 = 0;
		trig12 = 0;
		trig13 = 0;
		trig14 = 0;
		trig15 = 0;	
		
	  
	   u0 = 0;
	   u1 = 0;
	   u2 = 0;
	   u3 = 0;
	   u4 = 0;
	   u5 = 0;
	   u6 = 0;
	   u7 = 0;	   
	   u8 = 0;	
	   u9 = 0;	
	   u10 = 0;	
	   u11 = 0;	
	   u12 = 0;	
	   u13 = 0;	
	   u14 = 0;	
	   u15 = 0;		   
	   
	   v0 = 0;
	   v1 = 0;
	   v2 = 0;
	   v3 = 0;
	   v4 = 0;
	   v5 = 0;
	   v6 = 0;
	   v7 = 0;	
	   v8 = 0;
	   v9 = 0;
	   v10 = 0;
	   v11 = 0;
	   v12 = 0;
	   v13 = 0;
	   v14 = 0;
	   v15 = 0;
	   
       LPcount0 = 0;
	   LPcount1 = 0;
	   LPcount2 = 0; 
	   LPcount3 = 0;
       LPcount4 = 0;
	   LPcount5 = 0;
	   LPcount6 = 0; 
	   LPcount7 = 0;	
	   LPcount8 = 0;
	   LPcount9 = 0;
	   LPcount10 = 0;
	   LPcount11 = 0;
	   LPcount12 = 0;
	   LPcount13 = 0;
	   LPcount14 = 0;
	   LPcount15 = 0;	   
	  
	  
	  
	  
	  
	  c0 = 0;
	  c1 = 0;
	  c2 = 0;
	  c3 = 0;
	  c4 = 0;
	  c5 = 0;
	  c6 = 0;
	  c7 = 0;
	  c8 = 0;
	  c9 = 0;	 
	  c10 = 0;
	  c11 = 0;
	  c12 = 0;
		
	  s0 = 0;
	  s1 = 0;
	  s2 = 0;
	  s3 = 0;
	  
	  s4 = 0;
	  s5 = 0;
	  s6 = 0;
	  s7 = 0;
	  s8 = 0;
	  s9 = 0;
	  s10 = 0;
	  s11 = 0;	  
	  s12 = 0;	
	  s13 = 0;	
	  s14 = 0;	
	  s15 = 0;		  
	  
	  quotient = 0;
	  trig = 0;
	  
	  delay = 0;
	  delay2 = 0;
	  on = 0;
	  
	  flag1 = 0;
	  dt = 0;

	  ready = 0;
	  ninp = N*Nlaserpuls;
	  W = Wp*ninp;  
	  
	  Cnt = 0;
	  
	  //trigout0 = 0;
	  //trigout1 = 0;
	  //LPcount8 = 0;
	  
	end 
	
/* 	
	trigout0 = trigout1;
	trigout1 = rst;
	if(trigout0 && !trigout1) begin
		v8 = 1;
	
	end

	if(v8) begin
	
		LPcount8 = LPcount8 + 1;
	
	end 
	
	LPcount8 = LPcount8 + 1;
	
	if(LPcount8>27777) begin
	
		//v8 = 0;
		LPcount8 = 0;
	
	end	
	
	*/
	
	
/*	
	if(LPcount8>0 && LPcount8<=240) begin
		trig = 1'b1;
	end
	else begin
	
		trig = 1'b0;
	end
	
 	if(trigout1 && !trigout0) begin
		Cnt = 240; 
	
	end

	if(Cnt > 0)begin
		Cnt = Cnt - 1;	
	end
	
	
	trig = Cnt>0; */
			  
end 






endmodule
