CSDISM software package
Authors:
Shun Qin, shun.qin@phys.uni-goettingen.de
Sebastian Isbaner, sebastian.isbaner@phys.uni-goettingen.de
Jörg Enderlein, jenderl@gwdg.de

The folders contain the following software:
for users
-----------------
CSDISMusers/Built: contains the MicroManager Plugin for the National Instruments FPGA 7842R and the reconstruction software.
CSDISMusers/MMConfig: contains example configuration files for MicroManager.

MicroManager Plugin (folder: CSDISMusers/Built/ISMplugin)
1.  System requirements
OS: tested on Windows 7 and Windows 10, but any operating system that runs MicroManager 1.4 should work.
software required: MicroManager 1.4
required hardware: National Instruments FPGA
2.  Installation guide
see the "Readme.txt" inside the /Built/ISMplugin folder

Reconstruction software (folder: CSDISMusers/Built/ISMrecon)
1.  System requirements
OS: tested on Windows 7 and Windows 10, but any operating system that runs Java should work.
software required: Java Runtime Environment (JRE) of version at least 1.8.0_161.
2.  Installation guide
see the "Readme.txt" inside the /Built/ISMrecon folder
3.  Demo
see the "Readme.txt" inside the /Demo data/ folder

for developers
-----------------
CSDISMdevelopers/Project4OtherPlatforms: contains built files and source code for other NI FPGAs than the 7842R (experimental!).
CSDISMdevelopers/src: contains the source code for the MicroManager plugin and the reconstruction software.

Requirements to compile source code (folder: CSDISMdevelopers/src/)
1.  System requirements
software: Visual Studio 2012, MicroManager 1.4, LabView, eclipse, FPGA Interface C API 14.0, jama-1.0.3.jar, ij-1.52i.jar, ImageJ 1.52 
required hardware: National Instruments FPGA for the CSDISM plugin


Typical installation times for all compiled software is on the order of minutes, since only small files need to be copied to the correct folder by the user.

This open source package uses the following external programs / modules:
jama-1.0.3.jar,
ij-1.52i.jar,
MicroManager 1.4,
FPGA Interface C API 14.0 source file (Copyright (c) 2014 by National Instruments Corporation).