This folder contains the MicroManager plugin for CSDISM. 

Copy the files “ISMcore.dll” and “NiFpga_ISMcore.lvbitx” to the installation directory of Micro-Manager and copy the file “ISM.jar” to the folder “/mmplugins” in the same directory. After a successful installation, the plugin should show up in Micro-Manager in the plugins menu.