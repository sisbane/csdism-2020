This fodler contains two configuration files for MicroManager of our CSDISM system. They can be a starting point to configure MicroManager to your system.

Copy these configuration files onto your computer. Use MicroManager's Hardware Configuration Wizard (under Tools-->Hardware Configuration Wizard...) to open the file to open it and adapt it to your system.

Note that these configurations contain specifications of the magnification/pixel size of our optical system which can be very different from yours!