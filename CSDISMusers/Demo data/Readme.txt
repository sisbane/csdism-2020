This folder contains demo data for the ISM reconstruction.

"beads_250im_crop.tif" is a real dataset. It comprises a cropped CSDISM movie of spincoated TetraSpeck beads on a glass surface. The file "reference_250im_crop.tif" is the corresponding reference CSDISM movie.

1. Open the reconstruction software (ISMapp.jar, ISMPRO_x64.exe, or SDC_ISMpro-0-1-0.jar in /Built/ISMrecon/)
2. Set the following parameters:
pixel size: 108nm
FWHM: 400 nm
Background: 400
Slice Number: 1
nFrames/Slice: 250
Upsample scale: 4
3. In the line "sample", click on "Open..." and select the file "beads_250im_crop.tif" 
4. In the line "reference", click on "Open..." and select the file "reference_250im_crop.tif" 
5. Click on "Average". The average image (upscaled by a factor of 2) will be shown and saved as "AVG_beads_250im_crop.tif"
6. Click on "ISM". An animation of the reference pattern localization will be shown. Then, an animation of the ISM reconstruction is shown in a new window. Finally, the ISM image (upscaled by a factor of 2) will be shown and saved as "ISM_beads_250im_crop.tif". A file "reference_250im_crop.Localiz.txt" will be created to save the localized positions in order to speed up ISM reconstructions with the same reference file. This text file can be chosen as a reference as well.
7. compare the two images
8. You can also use the reference movie as a "sample". This will show the uniformity of the illumination. 

Computation times
Step 5: 2-5 seconds
Step 6: ~30 seconds for the localization + 10 seconds for the ISM reconstruction. After the localization has been successful and the "reference_250im_crop.Localiz.txt" has been created, the ISM reconstruction will take about 10 seconds for new sample files.